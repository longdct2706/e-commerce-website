<%-- 
    Document   : CatalogInsert
    Created on : Mar 22, 2019, 6:27:22 PM
    Author     : H
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<jsp:include page="Head.jsp"></jsp:include>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f" %>
<script>
    function validate() {
        var customername = document.getElementById("customername").value;
        var address = document.getElementById("address").value;
        var phone = document.getElementById("phone").value;
        var email = document.getElementById("email").value;
        if (customername != "" && address != "" && phone != "" && email != "") {

            return true;
        } else {

            if (customername == "") {
                alert("You have to input Customer Name.");
                document.getElementById("customername").focus();
                return false;
            }
            if (address == "") {
                alert("You have to input Address.");
                document.getElementById("address").focus();
                return false;
            }
            if (phone == "") {
                alert("You have to input Phone.");
                document.getElementById("phone").focus();
                return false;
            }
            if (phone == "") {
                alert("You have to input Email.");
                document.getElementById("email").focus();
                return false;
            }
        }

    }
</script>
<style>
    .message{
        color: #E50D0D;
    }
    .back{
        background-color: #00c0ef;
        border-color: #00acd6;
        border-radius: 3px;
        -webkit-box-shadow: none;
        box-shadow: none;
        border: 1px solid transparent;
        display: inline-block;
        padding: 6px 12px;
        margin-bottom: 0;
        font-size: 14px;
        font-weight: 400;
        line-height: 1.42857143;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        color: #fff;
        touch-action: manipulation;
        cursor: pointer;
        user-select: none;
        background-image: none;
    }
</style>
<html>
    <body class="hold-transition skin-blue sidebar-mini">
        <!-- Site wrapper -->
        <div class="wrapper">

            <jsp:include page="Header.jsp"></jsp:include>

                <!-- =============================================== -->

                <!-- Left side column. contains the sidebar -->
            <jsp:include page="Aside.jsp"></jsp:include>

                <!-- =============================================== -->
                <!-- Content Wrapper. Contains page content -->
                <div class="content-wrapper">
                    <!-- Content Header (Page header) -->
                    <section class="content-header">
                        <h1>
                            Order Page
                        </h1>
                        <ol class="breadcrumb">
                            <li><a href="getIndex.htm"><i class="fa fa-dashboard"></i> Home</a></li>
                            <li><a href="getAllOrder.htm">Order List</a></li>
                            <li class="active">Update Order</li>
                        </ol>
                    </section>
                    <!-- Main content -->
                    <section class="content">

                        <!-- Default box -->
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">Update Order</h3>
                                <h5 class=" message">
                                ${message}
                            </h5>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <f:form class="form-horizontal" action="updateOrder.htm" commandName="orderUpdate">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Order Id</label>
                                    <div class="col-sm-10">
                                        <f:input type="text" class="form-control" id="inputEmail3" placeholder="OrderId" path="orderId" readonly="true"/>
                                    </div>
                                </div>                  
                                <div class="form-group">
                                    <label for="customername" class="col-sm-2 control-label">Customer Name</label>
                                    <div class="col-sm-10">
                                        <f:input type="text" class="form-control" id="customername" placeholder="Customer Name" path="customerName"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Total Amount</label>
                                    <div class="col-sm-10">
                                        <f:input type="text" class="form-control" id="inputEmail3" placeholder="Total Amount" path="totalAmount" readonly="true"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="address" class="col-sm-2 control-label">Address</label>
                                    <div class="col-sm-10">
                                        <f:input type="text" class="form-control" id="address" placeholder="Address" path="address"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="phone" class="col-sm-2 control-label">Phone</label>
                                    <div class="col-sm-10">
                                        <f:input type="text" class="form-control" id="phone" placeholder="Phone" path="phone"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="phone" class="col-sm-2 control-label">Email</label>
                                    <div class="col-sm-10">
                                        <f:input type="text" class="form-control" id="email" placeholder="Email" path="email"/>
                                    </div>
                                </div>                                    
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Created Date</label>
                                    <div class="col-sm-10">
                                        <f:input type="text" class="form-control" id="inputEmail3" placeholder="Created Date" path="createdDate" readonly="true"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Status</label>
                                    <div class="col-sm-10">
                                        <f:select class="form-control" id="inputEmail3" placeholder="status" path="status">
                                            <f:option value="true">Delivered</f:option>
                                            <f:option value="false">Not Delivery</f:option>
                                        </f:select>
                                    </div>
                                </div>                  
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Payment Method</label>
                                    <div class="col-sm-10">
                                        <f:input type="text" class="form-control" id="inputEmail3" placeholder="Payment Method" path="paymentMethod" readonly="true"/>
                                    </div>
                                </div>                  
                            </div>

                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-info pull-right">Update</button>
                                <a href="getAllOrder.htm" class="back">Back</a>
                            </div>
                            <!-- /.box-footer -->
                        </f:form>
                    </div>
                    <!-- /.box -->
                    <!-- general form elements disabled -->

                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->

            <jsp:include page="Footer.jsp"></jsp:include>

            </div>
            <!-- ./wrapper -->

            <!-- jQuery 3 -->

        <jsp:include page="Script.jsp"></jsp:include>
    </body>
</html>
