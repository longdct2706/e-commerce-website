<%-- 
    Document   : Catalog
    Created on : Mar 21, 2019, 9:26:50 PM
    Author     : H
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<jsp:include page="Head.jsp"></jsp:include>
    <html>
        <body class="hold-transition skin-blue sidebar-mini">
            <!-- Site wrapper -->
            <div class="wrapper">

            <jsp:include page="Header.jsp"></jsp:include>

                <!-- =============================================== -->

                <!-- Left side column. contains the sidebar -->
            <jsp:include page="Aside.jsp"></jsp:include>

                <!-- =============================================== -->

                <!-- Content Wrapper. Contains page content -->
                <div class="content-wrapper">
                    <!-- Content Header (Page header) -->
                    <section class="content-header">
                        <h1>
                            Order Page
                        </h1>
                        <ol class="breadcrumb">
                            <li><a href="getIndex.htm"><i class="fa fa-dashboard"></i> Home</a></li>
                            <li><a href="#">Order Process</a></li>
                        </ol>
                    </section>

                    <!-- Main content -->
                    <section class="content">

                        <!-- Default box -->
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">List Order Process</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">             
                                <table id="order" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Order Id</th>
                                            <th>Customer Name</th>
                                            <th>Total Amount</th>
                                            <th>Address</th>
                                            <th>Phone</th>
                                            <th>Email</th>
                                            <th>Created Date</th>
                                            <th>Payment Method</th>
                                            <th>User Name</th>                                            
                                            <th>Status</th>                                            
                                            <th>Action</th> 
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach items="${listOrderProcess}" var="order">
                                        <tr>
                                            <td>${order.orderId}</td>
                                            <td>${order.customerName}</td>
                                            <td>${order.totalAmount}</td>
                                            <td>${order.address}</td>
                                            <td>${order.phone}</td>
                                            <td>${order.email}</td>
                                            <td>${order.createdDate}</td> 
                                            <td>${order.paymentMethod}</td>                                            
                                            <c:if test="${order.userName==null}">
                                                <td>None </td>
                                            </c:if>
                                            <c:if test="${order.userName!=null}">    
                                                <td>${order.userName}</td>
                                            </c:if>
                                            <c:if test="${order.status==true}">
                                                <td>Delivered</td>
                                            </c:if>
                                            <c:if test="${order.status==false}">
                                                <td>Not Deliver</td>
                                            </c:if>                                            
                                            <td>
                                                <a href="getOrderDetailByOrderId1.htm?orderId=${order.orderId}"><i class="fa fa-search-plus" aria-hidden="true"></i></a>
                                                <a href="initUpdateOrder1.htm?orderId=${order.orderId}"><i class="fa fa-refresh" aria-hidden="true"></i></a>
                                                <a href="deleteOrder1.htm?orderId=${order.orderId}"><i class="fa fa-trash-o"></i></a>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Order Id</th>
                                        <th>Customer Name</th>
                                        <th>Total Amount</th>
                                        <th>Address</th>
                                        <th>Phone</th>
                                        <th>Email</th>
                                        <th>Created Date</th>
                                        <th>Payment Method</th>
                                        <th>User Name</th>                                            
                                        <th>Status</th>                                            
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>

                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->

            <jsp:include page="Footer.jsp"></jsp:include>
            </div>
            <!-- ./wrapper -->

            <!-- jQuery 3 -->

        <jsp:include page="Script.jsp"></jsp:include>
        <script>
            $(function () {
                $('#order').DataTable({
                    'paging': true,
                    'lengthChange': false,
                    'searching': true,
                    'ordering': true,
                    'info': true,
                    'autoWidth': false
                })
            })
        </script>
    </body>
</html>

