<%-- 
    Document   : Catalog
    Created on : Mar 21, 2019, 9:26:50 PM
    Author     : H
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<jsp:include page="Head.jsp"></jsp:include>
    <html>
        <body class="hold-transition skin-blue sidebar-mini">
            <!-- Site wrapper -->
            <div class="wrapper">

            <jsp:include page="Header.jsp"></jsp:include>

                <!-- =============================================== -->

                <!-- Left side column. contains the sidebar -->
            <jsp:include page="Aside.jsp"></jsp:include>

                <!-- =============================================== -->

                <!-- Content Wrapper. Contains page content -->
                <div class="content-wrapper">
                    <!-- Content Header (Page header) -->
                    <section class="content-header">
                        <h1>
                            Income Page
                        </h1>
                        <ol class="breadcrumb">
                            <li><a href="getIndex.htm"><i class="fa fa-dashboard"></i> Home</a></li>
                            <li class="active">Income</li>
                        </ol>
                    </section>

                    <!-- Main content -->
                    <section class="content">

                        <!-- Default box -->
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Income</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">              
                                <table id="order" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Product Name</th>
                                            <th>Price</th>
                                            <th>Quantity</th>
                                            <th>Total Price</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach items="${listIncome}" var="income">
                                        <tr>
                                            <td>${income.productName}</td>
                                            <td>${income.price}</td>
                                            <td>${income.quantity}</td>
                                            <td>${income.totalPrice}</td>
                                        </c:forEach>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Product Name</th>
                                        <th>Price</th>
                                        <th>Quantity</th>
                                        <th>Total Price</th>          
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>

                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->

            <jsp:include page="Footer.jsp"></jsp:include>
            </div>
            <!-- ./wrapper -->

            <!-- jQuery 3 -->

        <jsp:include page="Script.jsp"></jsp:include>
        <script>
            $(function () {
                $('#order').DataTable({
                    'paging': true,
                    'lengthChange': false,
                    'searching': false,
                    'ordering': true,
                    'info': true,
                    'autoWidth': false
                })
            })
        </script>
    </body>
</html>

