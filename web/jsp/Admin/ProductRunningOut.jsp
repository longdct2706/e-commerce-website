<%-- 
    Document   : Catalog
    Created on : Mar 21, 2019, 9:26:50 PM
    Author     : H
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<jsp:include page="Head.jsp"></jsp:include>
    <style>
        .small-title{
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
            width: 20%;
        }
        table{
            table-layout: fixed;
        }
    </style>
    <html>
        <body class="hold-transition skin-blue sidebar-mini">
            <!-- Site wrapper -->
            <div class="wrapper">

            <jsp:include page="Header.jsp"></jsp:include>

                <!-- =============================================== -->

                <!-- Left side column. contains the sidebar -->
            <jsp:include page="Aside.jsp"></jsp:include>

                <!-- =============================================== -->

                <!-- Content Wrapper. Contains page content -->
                <div class="content-wrapper">
                    <!-- Content Header (Page header) -->
                    <section class="content-header">
                        <h1>
                            Product Page
                        </h1>
                        <ol class="breadcrumb">
                            <li><a href="getIndex.htm"><i class="fa fa-dashboard"></i> Home</a></li>
                            <li><a href="#">Running Out Products</a></li>
                        </ol>
                    </section>

                    <!-- Main content -->
                    <section class="content">

                        <!-- Default box -->
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Running Out Products</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">              
                                <table id="product" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Product Id</th>
                                            <th>Product Name</th>
                                            <th>Product Content</th>
                                            <th>Images</th>
                                            <th>Price</th>
                                            <th>Quantity</th>
                                            <th>Catalog</th>
                                            <th>Created</th>
                                            <th>Status</th>
                                            <th>Image Hover</th>
                                            <th>Discount</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach items="${listProduct}" var="product">
                                        <tr>
                                            <td>${product.productId}</td>
                                            <td>${product.productName}</td>
                                            <td class="small-title">${product.productContent}</td>
                                            <td><img src="/e-commerce-website/jsp/images/${product.images}" width="100px"></td>
                                            <td>${product.price}</td>
                                            <td>${product.quantity}</td>
                                            <td>
                                                <c:forEach items="${listCatalog}" var="cat">
                                                    <c:if test="${cat.catalogId==product.catalogId}">
                                                        ${cat.catalogName}
                                                    </c:if>
                                                </c:forEach>
                                            </td>
                                            <td>${product.created}</td>
                                            <c:if test="${product.status==true}">
                                                <td>Active</td>
                                            </c:if>
                                            <c:if test="${product.status==false}">
                                                <td>Not Active</td>
                                            </c:if> 
                                            <td><img src="/e-commerce-website/jsp/images/${product.imageHover}" width="100px"></td>
                                            <td>${product.discount}</td>                      
                                            <td>
                                                <a href="initUpdateProduct1.htm?productId=${product.productId}" title="Update"><i class="fa fa-refresh" aria-hidden="true"></i></a>
                                                <a href="deleteProduct1.htm?productId=${product.productId}" title="Delete"><i class="fa fa-trash-o"></i></a>
                                            </td>
                                        </c:forEach>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Product Id</th>
                                        <th>Product Name</th>
                                        <th>Product Content</th>
                                        <th>Images</th>
                                        <th>Price</th>
                                        <th>Quantity</th>                  
                                        <th>Catalog</th>
                                        <th>Created</th>
                                        <th>Status</th>
                                        <th>Image Hover</th>
                                        <th>Discount</th>
                                        <th>Action</th>              
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>

                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->

            <jsp:include page="Footer.jsp"></jsp:include>
            </div>
            <!-- ./wrapper -->

            <!-- jQuery 3 -->

        <jsp:include page="Script.jsp"></jsp:include>
        <script>
            $(function () {
                $('#product').DataTable({
                    'paging': true,
                    'lengthChange': true,
                    'searching': true,
                    'ordering': true,
                    'info': true,
                    'autoWidth': false
                })
            })
        </script>
    </body>
</html>

