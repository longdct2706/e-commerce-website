<%-- 
    Document   : CatalogInsert
    Created on : Mar 22, 2019, 6:27:22 PM
    Author     : H
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<jsp:include page="Head.jsp"></jsp:include>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f" %>
<script>
    function validate() {
        var catalogname = document.getElementById("catalogname").value;
        if (catalogname != "") {

            return true;
        } else {

            if (catalogname == "") {
                alert("You have to input Catalog Name");
                document.getElementById("catalogname").focus();
                return false;
            }
        }

    }
</script>
    <style>
        .message{
            color: #E50D0D;
        }
            .back{
        background-color: #00c0ef;
        border-color: #00acd6;
        border-radius: 3px;
        -webkit-box-shadow: none;
        box-shadow: none;
        border: 1px solid transparent;
        display: inline-block;
        padding: 6px 12px;
        margin-bottom: 0;
        font-size: 14px;
        font-weight: 400;
        line-height: 1.42857143;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        color: #fff;
        touch-action: manipulation;
        cursor: pointer;
        user-select: none;
        background-image: none;
    }
    </style>
<html>
    <body class="hold-transition skin-blue sidebar-mini">
        <!-- Site wrapper -->
        <div class="wrapper">

            <jsp:include page="Header.jsp"></jsp:include>

                <!-- =============================================== -->

                <!-- Left side column. contains the sidebar -->
            <jsp:include page="Aside.jsp"></jsp:include>

                <!-- =============================================== -->
                <!-- Content Wrapper. Contains page content -->
                <div class="content-wrapper">
                    <!-- Content Header (Page header) -->
                    <section class="content-header">
                        <h1>
                            Catalog Page
                        </h1>
                        <ol class="breadcrumb">
                            <li><a href="getIndex.htm"><i class="fa fa-dashboard"></i> Home</a></li>
                            <li><a href="getAllCatalog.htm">List Catalog</a></li>                          
                            <li><a href="#">Add New Catalog</a></li>
                        </ol>
                    </section>
                    <!-- Main content -->
                    <section class="content">

                        <!-- Default box -->
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">Add New Catalog</h3>
                                <h5 class=" message">
                                ${message}
                            </h5>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <f:form class="form-horizontal" action="insertCatalog.htm" commandName="catNew" onsubmit="return validate()">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="catalogname" class="col-sm-2 control-label">Catalog Name</label>
                                    <div class="col-sm-10">
                                        <f:input type="text" class="form-control" id="catalogname" placeholder="Name" path="catalogName"/>
                                    </div>
                                </div>

                            </div>

                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-info pull-right">Add</button>
                                <a href="getAllCatalog.htm" class="back">Back</a>
                            </div>
                            <!-- /.box-footer -->
                        </f:form>
                    </div>
                    <!-- /.box -->
                    <!-- general form elements disabled -->

                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->

            <jsp:include page="Footer.jsp"></jsp:include>

            </div>
            <!-- ./wrapper -->

            <!-- jQuery 3 -->

        <jsp:include page="Script.jsp"></jsp:include>
    </body>
</html>
