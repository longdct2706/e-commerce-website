<%-- 
    Document   : Login
    Created on : Jun 8, 2017, 1:46:39 PM
    Author     : TuyenMap
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta charset="utf-8" />
        <title>Login Page - Ace Admin</title>

        <meta name="description" content="User login page" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

        <!-- bootstrap & fontawesome -->
        <link rel="stylesheet" href="/ShoesDemo/jsp/Admin/css/bootstrap.min.css" />
        <link rel="stylesheet" href="/ShoesDemo/jsp/Admin/css/font-awesome.min.css" />

        <!-- text fonts -->
        <link rel="stylesheet" href="/ShoesDemo/jsp/Admin/fonts.googleapis.com.css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="/ShoesDemo/jsp/Admin/css/sweetalert.css" rel="stylesheet" type="text/css"/>
        <!-- ace styles -->
        <link rel="stylesheet" href="/ShoesDemo/jsp/Admin/css/ace.min.css" />
        <link href="/ShoesDemo/jsp/Admin/css/style.css" rel="stylesheet" type="text/css"/>

        <!--[if lte IE 9]>
                <link rel="stylesheet" href="assets/css/ace-part2.min.css" />
        <![endif]-->
        <link rel="stylesheet" href="assets/css/ace-rtl.min.css" />


        <!--[if lte IE 9]>
          <link rel="stylesheet" href="assets/css/ace-ie.min.css" />
        <![endif]-->

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

        <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
        <![endif]-->
        <style>
            .message{
                color: #E50D0D;
            }
        </style>
        <script>

            function validate() {
                var username = document.getElementById("username").value;
                var pass = document.getElementById("password").value;
                if (username != "" && pass != "") {

                    return true;
                } else {

                    if (username == "") {
                        alert("Bạn phải nhập tên đăng nhập.");
                        document.getElementById("username").focus();
                        return false;
                    }
                    if (pass == "") {
                        alert("Bạn phải nhập mật khẩu.");
                        document.getElementById("password").focus();
                        return false;
                    }
                }

            }
            function validatereg() {
                var usernamereg = document.getElementById("usernamereg").value;
                var passwordreg = document.getElementById("passwordreg").value;
                var emailreg = document.getElementById("emailreg").value;
                if (usernamereg != '' && passwordreg != '' && emailreg != '') {
                    return true;
                } else {
                    if (emailreg == '') {
                        alert("Bạn phải nhập email");
                        document.getElementById("emailreg").focus();
                        return false;
                    }
                    if (usernamereg == '') {
                        alert("Bạn phải nhập tên đăng nhập");
                        document.getElementById("usernamereg").focus();
                        return false;
                    }
                    if (passwordreg == '') {
                        alert("Bạn phải nhập mật khẩu");
                        document.getElementById("passwordreg").focus();
                        return false;
                    }
                }
            }
        </script>
    </head>

    <body class="login-layout">
        <div class="main-container">
            <div class="main-content">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1">
                        <div class="login-container">
                            <div class="center">
                                <h1>
                                    <i class="ace-icon fa fa-leaf green"></i>
                                    <span class="red">Login</span>
                                    <span class="white" id="id-text2">Admin</span>
                                </h1>
                                <h4 class="blue" id="id-company-text">Administrator Website</h4>
                            </div>
                            <div class="space-6"></div>

                            <div class="position-relative">
                                <div id="login-box" class="login-box visible widget-box no-border">
                                    <div class="widget-body">
                                        <div class="widget-main">
                                            <h4 class="header blue lighter bigger">
                                                <i class="ace-icon fa fa-coffee green"></i>
                                                Login Information
                                            </h4>

                                            <div class="space-6"></div>

                                            <form action="checkLoginAdmin.htm" onsubmit="return validate()" method="post">
                                                <fieldset>
                                                    <label class="block clearfix">
                                                        <div class="message">
                                                            ${message}
                                                        </div>
                                                    </label>
                                                    <label class="block clearfix">
                                                        <span class="block input-icon input-icon-right">
                                                            <input type="text" class="form-control" placeholder="User Name" name="userName" id="username" />
                                                            <i class="ace-icon fa fa-user"></i>
                                                        </span>
                                                    </label>

                                                    <label class="block clearfix">
                                                        <span class="block input-icon input-icon-right">
                                                            <input type="password" class="form-control" placeholder="Password" name="pass" id="password" />
                                                            <i class="ace-icon fa fa-lock"></i>
                                                        </span>
                                                    </label>

                                                    <div class="space"></div>

                                                    <div class="clearfix">

                                                        <button type="submit" class="width-35 pull-right btn btn-sm btn-primary">
                                                            <i class="ace-icon fa fa-key"></i>
                                                            <span class="bigger-110">Sign In</span>
                                                        </button>
                                                    </div>

                                                    <div class="space-4"></div>
                                                </fieldset>
                                            </form>

                                            <div class="space-6"></div>

                                        </div><!-- /.widget-main -->
                                    </div><!-- /.widget-body -->
                                </div><!-- /.login-box -->
                            </div><!-- /.position-relative -->

                            <div class="navbar-fixed-top align-right">
                                <br />
                                &nbsp;
                                <a id="btn-login-dark" href="#">Dark</a>
                                &nbsp;
                                <span class="blue">/</span>
                                &nbsp;
                                <a id="btn-login-blur" href="#">Blur</a>
                                &nbsp;
                                <span class="blue">/</span>
                                &nbsp;
                                <a id="btn-login-light" href="#">Light</a>
                                &nbsp; &nbsp; &nbsp;
                            </div>
                        </div>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.main-content -->
        </div><!-- /.main-container -->

        <!-- basic scripts -->

        <!--[if !IE]> -->
        <script src="/ShoesDemo/jsp/Admin/js/jquery.2.1.4.min.js"></script>

        <!-- <![endif]-->

        <!--[if IE]>
<script src="assets/js/jquery.1.11.1.min.js"></script>
<![endif]-->

        <!--[if !IE]> -->
        <script type="text/javascript">
                                                window.jQuery || document.write("<script src='/ShoesDemo/jsp/Admin/js/jquery.min.js'>" + "<" + "/script>");
        </script>

        <!-- <![endif]-->

        <!--[if IE]>
<script type="text/javascript">
window.jQuery || document.write("<script src='assets/js/jquery1x.min.js'>"+"<"+"/script>");
</script>
<![endif]-->
        <script type="text/javascript">
            if ('ontouchstart' in document.documentElement)
                document.write("<script src='/ShoesDemo/jsp/Admin/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
        </script>
        <script src="/ShoesDemo/jsp/Admin/js/sweetalert.min.js" type="text/javascript"></script>
        <!-- inline scripts related to this page -->
        <script type="text/javascript">




            //you don't need this, just used for changing background
            jQuery(function ($) {
                $('#btn-login-dark').on('click', function (e) {
                    $('body').attr('class', 'login-layout');
                    $('#id-text2').attr('class', 'white');
                    $('#id-company-text').attr('class', 'blue');

                    e.preventDefault();
                });
                $('#btn-login-light').on('click', function (e) {
                    $('body').attr('class', 'login-layout light-login');
                    $('#id-text2').attr('class', 'grey');
                    $('#id-company-text').attr('class', 'blue');

                    e.preventDefault();
                });
                $('#btn-login-blur').on('click', function (e) {
                    $('body').attr('class', 'login-layout blur-login');
                    $('#id-text2').attr('class', 'white');
                    $('#id-company-text').attr('class', 'light-blue');

                    e.preventDefault();
                });

            });
        </script>
    </body>
</html>

