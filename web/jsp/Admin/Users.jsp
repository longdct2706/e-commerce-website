<%-- 
    Document   : Catalog
    Created on : Mar 21, 2019, 9:26:50 PM
    Author     : H
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<jsp:include page="Head.jsp"></jsp:include>
    <html>
        <body class="hold-transition skin-blue sidebar-mini">
            <!-- Site wrapper -->
            <div class="wrapper">

            <jsp:include page="Header.jsp"></jsp:include>

                <!-- =============================================== -->

                <!-- Left side column. contains the sidebar -->
            <jsp:include page="Aside.jsp"></jsp:include>

                <!-- =============================================== -->

                <!-- Content Wrapper. Contains page content -->
                <div class="content-wrapper">
                    <!-- Content Header (Page header) -->
                    <section class="content-header">
                        <h1>
                            User Page
                        </h1>
                        <ol class="breadcrumb">
                            <li><a href="getIndex.htm"><i class="fa fa-dashboard"></i> Home</a></li>
                            <li><a href="#">List User</a></li>
                        </ol>
                    </section>

                    <!-- Main content -->
                    <section class="content">

                        <!-- Default box -->
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">List User</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">              
                                <table id="users" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>User Id</th>
                                            <th>User Name</th>
                                            <th>Password</th>
                                            <th>Email</th>
                                            <th>Address</th>
                                            <th>Phone</th>
                                            <th>Created Date</th>
                                            <th>Type</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach items="${listUsers}" var="user">
                                        <tr>
                                            <td>${user.userId}</td>
                                            <td>${user.userName}</td>
                                            <td>${user.pass}</td>
                                            <td>${user.email}</td>
                                            <td>${user.address}</td>
                                            <td>${user.phone}</td>
                                            <td>${user.created}</td>
                                            <c:if test="${user.isAdmin==true}">
                                                <td>Admin</td>
                                            </c:if>
                                            <c:if test="${user.isAdmin==false}">
                                                <td>User</td>
                                            </c:if>                      
                                            <c:if test="${user.status==true}">
                                                <td>Active</td>
                                            </c:if>
                                            <c:if test="${user.status==false}">
                                                <td>Not Active</td>
                                            </c:if>                      
                                        </c:forEach>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>User Id</th>
                                        <th>User Name</th>
                                        <th>Password</th>
                                        <th>Email</th>
                                        <th>Address</th>
                                        <th>Phone</th>
                                        <th>Created Date</th>
                                        <th>Type</th>
                                        <th>Status</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>

                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->

            <jsp:include page="Footer.jsp"></jsp:include>
            </div>
            <!-- ./wrapper -->

            <!-- jQuery 3 -->

        <jsp:include page="Script.jsp"></jsp:include>
        <script>
            $(function () {
                $('#users').DataTable({
                    'paging': true,
                    'lengthChange': true,
                    'searching': true,
                    'ordering': true,
                    'info': true,
                    'autoWidth': false
                })
            })
        </script>
    </body>
</html>

