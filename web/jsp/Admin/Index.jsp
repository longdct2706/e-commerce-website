<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<jsp:include page="Head.jsp"></jsp:include>
    <style>
        .small-box h3 {
            font-size: 25px;
            font-weight: bold;
            margin: 0 0 15px 0;
            white-space: nowrap;
            padding: 0;
        }
        .small-box-footer1{
            position: relative;
            text-align: center;
            padding: 0px 5px;
            color: #fff;
            color: rgba(255, 255, 255, 0.8);
            z-index: 10;
            background: rgba(0, 0, 0, 0.1);
            text-decoration: none;
            display: block;
        }
    </style>
    <body class="hold-transition skin-blue sidebar-mini">
        <!-- Site wrapper -->
        <div class="wrapper">

        <jsp:include page="Header.jsp"></jsp:include>

            <!-- =============================================== -->

            <!-- Left side column. contains the sidebar -->
        <jsp:include page="Aside.jsp"></jsp:include>

            <!-- =============================================== -->

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Dashboard
                        <small>Control panel</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home </a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <!-- Small boxes (Stat box) -->
                    <div class="row">
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-aqua">
                                <div class="inner">
                                    <h3>${totalOrderProcess}</h3>

                                <p>Order Process</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-bag"></i>
                            </div>
                            <a href="getListOrderProcess.htm" class="small-box-footer">Detail<i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-green">
                            <div class="inner">
                                <h3>${totalProductRunningOut}</h3>

                                <p>Running Out Product</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-stats-bars"></i>
                            </div>
                            <a href="getProductRunningOut.htm" class="small-box-footer">Detail<i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-yellow">
                            <div class="inner">
                                <h3>${totalCustomer}</h3>

                                <p>Total Customer</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-person-add"></i>
                            </div>
                            <a href="getListCustomer.htm" class="small-box-footer">Detail<i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-red">
                            <div class="inner">
                                <h3>${year}</h3>

                                <p>Revenue</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-pie-graph"></i>
                            </div>
                            <c:if test="${month>=1}">
                                <a href="getListIncome.htm?month=1" class="small-box-footer1">January: $<fmt:formatNumber  value="${totalIncome1}"></fmt:formatNumber></a>
                            </c:if>
                            <c:if test="${month>=2}">
                                <a href="getListIncome.htm?month=2" class="small-box-footer1">February: $<fmt:formatNumber  value="${totalIncome2}"></fmt:formatNumber></a>
                            </c:if>
                            <c:if test="${month>=3}">
                                <a href="getListIncome.htm?month=3" class="small-box-footer1">March: $<fmt:formatNumber  value="${totalIncome3}"></fmt:formatNumber></a>
                            </c:if>
                            <c:if test="${month>=4}">
                                <a href="getListIncome.htm?month=4" class="small-box-footer1">April: $<fmt:formatNumber  value="${totalIncome4}"></fmt:formatNumber></a>
                            </c:if>
                            <c:if test="${month>=5}">
                                <a href="getListIncome.htm?month=5" class="small-box-footer1">May: $<fmt:formatNumber  value="${totalIncome5}"></fmt:formatNumber></a>
                            </c:if>
                            <c:if test="${month>=6}">
                                <a href="getListIncome.htm?month=6" class="small-box-footer1">June: $<fmt:formatNumber  value="${totalIncome6}"></fmt:formatNumber></a>
                            </c:if>
                            <c:if test="${month>=7}">
                                <a href="getListIncome.htm?month=7" class="small-box-footer1">July: $<fmt:formatNumber  value="${totalIncome7}"></fmt:formatNumber></a>
                            </c:if>
                            <c:if test="${month>=8}">
                                <a href="getListIncome.htm?month=8" class="small-box-footer1">August: $<fmt:formatNumber  value="${totalIncome8}"></fmt:formatNumber></a>
                            </c:if>
                            <c:if test="${month>=9}">
                                <a href="getListIncome.htm?month=9" class="small-box-footer1">September: $<fmt:formatNumber  value="${totalIncome9}"></fmt:formatNumber></a>
                            </c:if>
                            <c:if test="${month>=10}">
                                <a href="getListIncome.htm?month=10" class="small-box-footer1">October: $<fmt:formatNumber  value="${totalIncome10}"></fmt:formatNumber></a>
                            </c:if>
                            <c:if test="${month>=11}">
                                <a href="getListIncome.htm?month=11" class="small-box-footer1">November: $<fmt:formatNumber  value="${totalIncome11}"></fmt:formatNumber></a>
                            </c:if>
                            <c:if test="${month>=12}">
                                <a href="getListIncome.htm?month=12" class="small-box-footer1">December: $<fmt:formatNumber  value="${totalIncome12}"></fmt:formatNumber></a>
                            </c:if>
                        </div>
                    </div>
                    <!-- ./col -->
                </div>
                <!-- /.row -->
                <!-- Main row -->

                <!-- /.row (main row) -->

            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <jsp:include page="Footer.jsp"></jsp:include>
        </div>
        <!-- ./wrapper -->

        <!-- jQuery 3 -->

    <jsp:include page="Script.jsp"></jsp:include>
</body>
</html>
