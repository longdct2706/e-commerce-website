<%-- 
    Document   : Catalog
    Created on : Mar 21, 2019, 9:26:50 PM
    Author     : H
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<jsp:include page="Head.jsp"></jsp:include>
<html>
<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

<jsp:include page="Header.jsp"></jsp:include>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
<jsp:include page="Aside.jsp"></jsp:include>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Catalog Page
      </h1>
      <ol class="breadcrumb">
        <li><a href="getIndex.htm"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">List Catalog</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
       <div class="box">
            <div class="box-header">
              <h3 class="box-title">List Catalog</h3>
              <h5><a href="initInsertCatalog.htm">Create New Catalog</a></h5>
            </div>
            <!-- /.box-header -->
            <div class="box-body">               
              <table id="catalog" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Catalog Id</th>
                  <th>Catalog Name</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${listCatalog}" var="catalog">
                <tr>
                  <td>${catalog.catalogId}</td>
                  <td>${catalog.catalogName}</td>
                  <c:if test="${catalog.status==true}">
                      <td>Active</td>
                  </c:if>
                  <c:if test="${catalog.status==false}">
                      <td>Not Active</td>
                  </c:if>                      
                  <td>
                      <a href="initUpdateCatalog.htm?catalogId=${catalog.catalogId}" title="Update"><i class="fa fa-refresh" aria-hidden="true"></i></a>
                      <a href="deleteCatalog.htm?catalogId=${catalog.catalogId}" title="Delete"><i class="fa fa-trash-o"></i></a>
                  </td>
                </c:forEach>
                </tbody>
                <tfoot>
                <tr>
                  <th>Catalog Id</th>
                  <th>Catalog Name</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <jsp:include page="Footer.jsp"></jsp:include>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->

  <jsp:include page="Script.jsp"></jsp:include>
<script>
  $(function () {
    $('#catalog').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>

