<%-- 
    Document   : CatalogInsert
    Created on : Mar 22, 2019, 6:27:22 PM
    Author     : H
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<jsp:include page="Head.jsp"></jsp:include>
    <script src="/e-commerce-website/jsp/ckeditor/ckeditor.js" type="text/javascript"></script>  
    <script src="/e-commerce-website/jsp/ckfinder/ckfinder.js" type="text/javascript"></script> 
    <script>
        function validate() {
            var productname = document.getElementById("productname").value;
            var images = document.getElementById("images").value;
            var price = document.getElementById("price").value;
            var quantity = document.getElementById("quantity").value;
            var imagehover = document.getElementById("imagehover").value;
            var discount = document.getElementById("discount").value;
            if (productname != "" && images != "" && price != "" && imagehover != "" && discount != "" && quantity != "") {

                return true;
            } else {

                if (productname == "") {
                    alert("You have to input Product Name.");
                    document.getElementById("productname").focus();
                    return false;
                }
                if (images == "") {
                    alert("You have to input Images");
                    document.getElementById("images").focus();
                    return false;
                }
                if (price == "") {
                    alert("You have to input Price");
                    document.getElementById("price").focus();
                    return false;
                }
                if (quantity == "") {
                    alert("You have to input Quantity");
                    document.getElementById("quantity").focus();
                    return false;
                }
                if (imagehover == "") {
                    alert("You have to input ImageHover");
                    document.getElementById("imagehover").focus();
                    return false;
                }
                if (discount == "") {
                    alert("You have to input Discount");
                    document.getElementById("discount").focus();
                    return false;
                }
            }

        }
    </script>
    <style>
        .message{
            color: #E50D0D;
        }
        .back{
            background-color: #00c0ef;
            border-color: #00acd6;
            border-radius: 3px;
            -webkit-box-shadow: none;
            box-shadow: none;
            border: 1px solid transparent;
            display: inline-block;
            padding: 6px 12px;
            margin-bottom: 0;
            font-size: 14px;
            font-weight: 400;
            line-height: 1.42857143;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            color: #fff;
            touch-action: manipulation;
            cursor: pointer;
            user-select: none;
            background-image: none;
        }
    </style>
    <html>
        <body class="hold-transition skin-blue sidebar-mini">
            <!-- Site wrapper -->
            <div class="wrapper">

            <jsp:include page="Header.jsp"></jsp:include>

                <!-- =============================================== -->

                <!-- Left side column. contains the sidebar -->
            <jsp:include page="Aside.jsp"></jsp:include>

                <!-- =============================================== -->
                <!-- Content Wrapper. Contains page content -->
                <div class="content-wrapper">
                    <!-- Content Header (Page header) -->
                    <section class="content-header">
                        <h1>
                            Product Page
                        </h1>
                        <ol class="breadcrumb">
                            <li><a href="getIndex.htm"><i class="fa fa-dashboard"></i>Home</a></li>
                            <li><a href="#">Add New Product</a></li>
                        </ol>
                    </section>
                    <!-- Main content -->
                    <section class="content">

                        <!-- Default box -->
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">Add New Product</h3>
                                <h5 class=" message">
                                ${message}
                            </h5>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <f:form class="form-horizontal" action="insertProduct.htm" commandName="proNew" enctype="multipart/form-data" onsubmit="return validate()">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="productname" class="col-sm-2 control-label">Product Name</label>
                                    <div class="col-sm-10">
                                        <f:input type="text" class="form-control" id="productname" placeholder="ProductName" path="productName"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="productcontent" class="col-sm-2 control-label">Product Content</label>
                                    <div class="col-sm-10">
                                        <f:textarea class="form-control" id="productcontent" placeholder="ProductContent" path="productContent"/>
                                    </div>
                                </div>                                   
                                <div class="form-group">
                                    <label for="images" class="col-sm-2 control-label">Images</label>
                                    <div class="col-sm-10">
                                        <f:input type="file" class="form-control" id="images" placeholder="Images" path="images"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="price" class="col-sm-2 control-label">Price</label>
                                    <div class="col-sm-10">
                                        <f:input type="number" class="form-control" id="price" placeholder="Price" path="price" min="0" step="0.01" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="quantity" class="col-sm-2 control-label">Quantity</label>
                                    <div class="col-sm-10">
                                        <f:input type="number" class="form-control" id="quantity" placeholder="Quantity" path="quantity" min="0"/>
                                    </div>
                                </div>                                    
                                <div class="form-group">
                                    <label for="catalogid" class="col-sm-2 control-label">Catalog</label>
                                    <div class="col-sm-10">
                                        <f:select  class="form-control" id="catalogid" placeholder="CatalogId" path="catalogId">
                                            <c:forEach items="${listCatalog}" var="cat">
                                                <f:option value="${cat.catalogId}">${cat.catalogName}</f:option>
                                            </c:forEach>
                                        </f:select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="imagehover" class="col-sm-2 control-label">Image Hover</label>
                                    <div class="col-sm-10">
                                        <f:input type="file" class="form-control" id="imagehover" placeholder="ImageHover" path="imageHover"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="discount" class="col-sm-2 control-label">Discount</label>
                                    <div class="col-sm-10">
                                        <f:input type="number" class="form-control" id="discount" placeholder="Discount" path="discount" min="0" max="100" />
                                    </div>
                                </div>                  
                            </div>

                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-info pull-right">Add</button>
                                <a href="getAllProduct.htm" class="back">Back</a>
                            </div>
                            <!-- /.box-footer -->
                        </f:form>
                    </div>
                    <!-- /.box -->
                    <!-- general form elements disabled -->

                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->

            <jsp:include page="Footer.jsp"></jsp:include>

            </div>
            <!-- ./wrapper -->

            <!-- jQuery 3 -->

        <jsp:include page="Script.jsp"></jsp:include>
        <script type="text/javascript">
            var editorProductContent = CKEDITOR.replace('productContent');
            CKFinder.setupCKEditor(editorProductContent, 'jsp/ckfinder');
        </script>
    </body>
</html>
