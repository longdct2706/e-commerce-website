<%-- 
    Document   : Aside
    Addd on : Mar 21, 2019, 9:35:45 PM
    Author     : H
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="/e-commerce-website/jsp/images/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>${account}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->

        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li>
                <a href="getIndex.htm">
                    <i class="fa fa-dashboard"></i> <span>Home</span>
                    <span class="pull-right-container">
                        
                    </span>
                </a>
            </li>

            <li >
                <a href="getAllCatalog.htm">
                    <i class="fa fa-th"></i> <span>Catalog</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
            </li>
            <li >
                <a href="getAllProduct.htm">
                    <i class="fa fa-th"></i> <span>Product</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>                
            </li>              
            <li>
                <a href="getAllUsers.htm">
                    <i class="fa fa-th"></i> <span>Users</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
            </li>
            <li>
                <a href="getAllOrder.htm">
                    <i class="fa fa-th"></i> <span>Order</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
            </li>         
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>