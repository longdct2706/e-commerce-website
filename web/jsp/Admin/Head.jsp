<%-- 
    Document   : Head
    Created on : Mar 21, 2019, 9:21:50 PM
    Author     : H
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Blank Page</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="/e-commerce-website/jsp/Admin/css/bootstrap.min.css">
  <link rel="stylesheet" href="/e-commerce-website/jsp/Admin/css/font-awesome.min.css">
  <link rel="stylesheet" href="/e-commerce-website/jsp/Admin/css/AdminLTE.css">
  <link rel="stylesheet" href="/e-commerce-website/jsp/Admin/css/_all-skins.min.css">
  <link rel="stylesheet" href="/e-commerce-website/jsp/Admin/css/jquery-ui.css">
  <link rel="stylesheet" href="/e-commerce-website/jsp/Admin/css/style.css" />
  <link rel="stylesheet" href="/e-commerce-website/jsp/Admin/css/dataTables.bootstrap.min.css">

  <script src="/e-commerce-website/jsp/Admin/js/angular.min.js"></script>
  <script src="/e-commerce-website/jsp/Admin/js/app.js"></script>
</head>
