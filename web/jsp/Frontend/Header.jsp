<%-- 
    Document   : Header
    Created on : Apr 25, 2019, 9:54:15 AM
    Author     : H
--%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<style>
    .meninmenu li.drop .megamenu {
        width: 240px;
    }
    .meninmenu li.drop .megamenu .item li a:hover {
    padding-left: 0px;
}
.meninmenu li.drop .megamenu .item.item03 {
     flex-basis: 100%; 
}
</style>
<header id="wn__header" class="oth-page header__area header__absolute sticky__header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4 col-sm-4 col-7 col-lg-2">
                <div class="logo">
                    <a href="index.html">
                        <img src="/e-commerce-website/jsp/Frontend/images/logo/logo.png" alt="logo images">
                    </a>
                </div>
            </div>
            <div class="col-lg-8 d-none d-lg-block">
                <nav class="mainmenu__nav">
                    <ul class="meninmenu d-flex justify-content-start">
                        <li class="drop with--one--item"><a href="getAllProduct.htm">Home</a>
                        </li>
                        <li class="drop"><a href="#">Shop</a>
                            <div class="megamenu mega02">
                                <ul class="item item02">
                                    <li class="title">Shop Layout</li>
                                    <li><a href="getAllProductShopList.htm">Shop List</a></li>
                                    <li><a href="addCart.htm">Shopping Cart</a></li>
                                    <li><a href="initCheckOut.htm">Checkout</a></li>
                                </ul>
                                <ul class="item item02">
                                    <li class="title">Shop Page</li>
                                    <c:if test="${account==null}">
                                    <li><a href="initLogin.htm">Log In</a></li>
                                    </c:if>
                                    <c:if test="${account!=null}">
                                    <li><a href="logout.htm">Log Out</a></li>                                    
                                    </c:if>
                                    <li><a href="initRegister.htm">Register</a></li>
                                </ul>
                            </div>
                        </li>
                        <li class="drop"><a href="#">Books</a>
                            <div class="megamenu" >
                                <ul class="item item03">
                                    <li class="title">Categories</li>
                                    <c:forEach items="${listCatalog}" var="cat">
                                    <li><a href="getProductByCatalogId.htm?catalogId=${cat.catalogId}">${cat.catalogName} </a></li>
                                    </c:forEach>
                                </ul>
                            </div>
                        </li>                        
                        <li class="drop"><a href="#">Pages</a>
                            <div class="megamenu dropdown">
                                <ul class="item item01">
                                    <li><a href="getAllProductShopList.htm">Shop List</a></li>
                                    <li><a href="addCart.htm">Shopping Cart</a></li>
                                    <li><a href="initCheckOut.htm">Checkout</a></li>
                                    <c:if test="${account==null}">
                                    <li><a href="initLogin.htm">Log In</a></li>
                                    </c:if>
                                    <c:if test="${account!=null}">
                                    <li><a href="logout.htm">Log Out</a></li>                                    
                                    </c:if>
                                    <li><a href="initRegister.htm">Register</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="col-md-8 col-sm-8 col-5 col-lg-2">
                <ul class="header__sidebar__right d-flex justify-content-end align-items-center">
                    <li class="shopcart"><a class="cartbox_active" href="#"><span class="product_qun">${listCart.size()}</span></a>
                        <!-- Start Shopping Cart -->
                        <div class="block-minicart minicart__active">
                            <div class="minicart-content-wrapper">
                                <div class="micart__close">
                                    <span>close</span>
                                </div>
                                <div class="items-total d-flex justify-content-between">
                                    <span>${listCart.size()} items</span>
                                    <span>Cart Subtotal</span>
                                </div>
                                    <div class="total_amount text-right">
                                    <span>$<fmt:formatNumber  value="${totalAmount}"></fmt:formatNumber></span>
                                </div>
                                <div class="mini_action checkout">
                                    <a class="checkout__btn" href="initCheckOut.htm">Go to Checkout</a>
                                </div>
                                <div class="single__items">
                                    <div class="miniproduct">
                                        <c:forEach items="${listCart}" var="cart">
                                        <div class="item01 d-flex">
                                            <div class="thumb">
                                                <a href="initSingleProduct.htm?productId=${cart.product.productId}"><img src="/e-commerce-website/jsp/images/${cart.product.images}" alt="product images"></a>
                                            </div>
                                            <div class="content">
                                                <h6><a href="initSingleProduct.htm?productId=${cart.product.productId}">${cart.product.productName}</a></h6>
                                                <span class="prize">$<fmt:formatNumber  value="${cart.product.price*(100-cart.product.discount)/100}"></fmt:formatNumber></span>
                                                <div class="product_prize d-flex justify-content-between">
                                                    <span class="qun">Qty: ${cart.quantity}</span>
                                                    <ul class="d-flex justify-content-end">
                                                        <li><a href="removeCart.htm?productId=${cart.product.productId}"><i class="zmdi zmdi-delete"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        </c:forEach>
                                    </div>
                                </div>
                                <div class="mini_action cart">
                                    <a class="cart__btn" href="addCart.htm">View and edit cart</a>
                                </div>
                            </div>
                        </div>
                        <!-- End Shopping Cart -->
                    </li>
                    <li class="setting__bar__icon"><a class="setting__active" href="#"></a>
                        <div class="searchbar__content setting__block">
                            <div class="content-inner">
                                <div class="switcher-currency">
                                    <strong class="label switcher-label">
                                    <c:if test="${account==null}">
                                        <span>My Account</span>
                                    </c:if>
                                    <c:if test="${account !=null}">
                                        <span>Welcome ${account} !</span>
                                    </c:if>
                                    </strong>
                                    <div class="switcher-options">
                                        <div class="switcher-currency-trigger">
                                            <div class="setting__menu">
                                            <c:if test="${account==null}">
                                                <span><a href="initLogin.htm">Sign In</a></span>
                                            </c:if>
                                            <c:if test="${account !=null}">
                                                <span><a href="logout.htm">Sign Out</a></span>
                                                <span><a href="getOrderHistory.htm">Order History</a></span>                                                
                                            </c:if>
                                                <span><a href="initRegister.htm">Create An Account</a></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <!-- Start Mobile Menu -->
        <div class="row d-none">
            <div class="col-lg-12 d-none">
                <nav class="mobilemenu__nav">
                    <ul class="meninmenu">
                        <li><a href="index.html">Home</a>
                            <ul>
                                <li><a href="index.html">Home Style Default</a></li>
                                <li><a href="index-2.html">Home Style Two</a></li>
                                <li><a href="index-box.html">Home Box Style</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Pages</a>
                            <ul>
                                <li><a href="about.html">About Page</a></li>
                                <li><a href="portfolio.html">Portfolio</a>
                                    <ul>
                                        <li><a href="portfolio.html">Portfolio</a></li>
                                        <li><a href="portfolio-three-column.html">Portfolio 3 Column</a></li>
                                        <li><a href="portfolio-details.html">Portfolio Details</a></li>
                                    </ul>
                                </li>
                                <li><a href="my-account.html">My Account</a></li>
                                <li><a href="cart.html">Cart Page</a></li>
                                <li><a href="checkout.html">Checkout Page</a></li>
                                <li><a href="wishlist.html">Wishlist Page</a></li>
                                <li><a href="error404.html">404 Page</a></li>
                                <li><a href="faq.html">Faq Page</a></li>
                                <li><a href="team.html">Team Page</a></li>
                            </ul>
                        </li>
                        <li><a href="shop-grid.html">Shop</a>
                            <ul>
                                <li><a href="shop-grid.html">Shop Grid</a></li>
                                <li><a href="shop-list.html">Shop List</a></li>
                                <li><a href="shop-left-sidebar.html">Shop Left Sidebar</a></li>
                                <li><a href="shop-right-sidebar.html">Shop Right Sidebar</a></li>
                                <li><a href="shop-no-sidebar.html">Shop No sidebar</a></li>
                                <li><a href="single-product.html">Single Product</a></li>
                            </ul>
                        </li>
                        <li><a href="blog.html">Blog</a>
                            <ul>
                                <li><a href="blog.html">Blog Page</a></li>
                                <li><a href="blog-left-sidebar.html">Blog Left Sidebar</a></li>
                                <li><a href="blog-no-sidebar.html">Blog No Sidebar</a></li>
                                <li><a href="blog-details.html">Blog Details</a></li>
                            </ul>
                        </li>
                        <li><a href="contact.html">Contact</a></li>
                    </ul>
                </nav>
            </div>
        </div>
        <!-- End Mobile Menu -->
        <div class="mobile-menu d-block d-lg-none">
        </div>
        <!-- Mobile Menu -->	
    </div>		
</header>
