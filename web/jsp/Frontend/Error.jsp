<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="no-js" lang="">

    <!-- Mirrored from demo.devitems.com/boighor-v2/shop-list.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 04 Apr 2019 10:36:44 GMT -->
    <jsp:include page="Head.jsp"></jsp:include>
        <style>
            .bg-image--5 {
                background-image: url(/e-commerce-website/jsp/Frontend/images/bg/5.jpg);
                background-repeat: no-repeat;
                background-size: cover;
                background-position: center center; }
            </style>
            <body>
                <!--[if lte IE 9]>
                        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
                <![endif]-->

                <!-- Main wrapper -->
                <div class="wrapper" id="wrapper">

                <!-- Header -->
            <jsp:include page="Header.jsp"></jsp:include>
                <!-- //Header -->
                <!-- Start Search Popup -->
                <!-- End Search Popup -->
                <!-- Start Bradcaump area -->
                <!-- End Search Popup -->
                <!-- Start Bradcaump area -->
                <div class="ht__bradcaump__area bg-image--5">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="bradcaump__inner text-center">
                                    <h2 class="bradcaump-title">404</h2>
                                    <nav class="bradcaump-content">
                                        <a class="breadcrumb_item" href="index.html">Home</a>
                                        <span class="brd-separetor">/</span>
                                        <span class="breadcrumb_item active">404</span>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Bradcaump area -->

                <!-- Start Error Area -->
                <section class="page_error section-padding--lg bg--white">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="error__inner text-center">
                                    <div class="error__content">
                                        <h2>You have ordered failed !</h2>
                                        <p>${message}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- End Error Area -->

                <!-- Footer Area -->
            <jsp:include page="Footer.jsp"></jsp:include>
                <!-- //Footer Area -->

            </div>
            <!-- //Main wrapper -->

            <!-- JS Files -->
        <jsp:include page="Script.jsp"></jsp:include>

    </body>

    <!-- Mirrored from demo.devitems.com/boighor-v2/error404.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 04 Apr 2019 10:38:32 GMT -->
</html>