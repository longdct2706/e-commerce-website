<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="no-js" lang="">

    <!-- Mirrored from demo.devitems.com/boighor-v2/shop-list.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 04 Apr 2019 10:36:44 GMT -->
    <jsp:include page="Head.jsp"></jsp:include>
        <style>
            .buttons-set button {
                background: transparent none repeat scroll 0 0;
                border: 1px solid #666666;
                border-radius: 20px;
                color: #666666;
                font-weight: 500;
                padding: 10px 30px;
            }
            .buttons-set {
                border-top: 1px solid #f2f2f2;
                clear: both;
                margin: 20px 0 0;
                padding: 8px 0 0;
                text-align: right;
            }
            .buttons-set button:hover{
                background: #FF7575 none repeat scroll 0 0;
                color:#fff;
                border-color:#FF7575;
            }
        </style>
        <script>
            function validate() {
                var customername = document.getElementById("customerName").value;
                var email = document.getElementById("email").value;
                var address = document.getElementById("address").value;
                var phone = document.getElementById("phone").value;
                var check = true;
                var i;
                for (i = 0; i < phone.length; i++) {
                    if (phone.charCodeAt(0) != 48) {
                        check = false;
                        break;
                    }
                    if (phone.charCodeAt(i) < 48 || phone.charCodeAt(i) > 57) {
                        check = false;
                        break;
                    }
                }
                if (customername != "" && address != "" && phone != "" && check == true && phone.length == 10 && email != "") {
                    return true;
                } else {
                    if (customername == "") {
                        alert("You have to input your Fullname");
                        document.getElementById("customerName").focus();
                    }else if (address == "") {
                        alert("You have to input your Address");
                        document.getElementById("address").focus();
                    }else if (phone == "") {
                        alert("You have to input your Phone");
                        document.getElementById("phone").focus();
                    }else if (check != true) {
                        alert("Phone only have numbers from 0-9 and start from 0");
                        document.getElementById("phone").focus();
                    }else if (phone.length != 10) {
                        alert("Phone must have 10 numbers");
                        document.getElementById("phone").focus();
                    }else if (email == "") {
                        alert("You have to input your Email");
                        document.getElementById("email").focus();
                    }
                    return false;
                }
            }
        </script>
        <body>
            <!--[if lte IE 9]>
                    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
            <![endif]-->

            <!-- Main wrapper -->
            <div class="wrapper" id="wrapper">

                <!-- Header -->
            <jsp:include page="Header.jsp"></jsp:include>
                <!-- //Header -->
                <!-- Start Search Popup -->
                <div class="box-search-content search_active block-bg close__top">
                    <form id="search_mini_form" class="minisearch" action="#">
                        <div class="field__search">
                            <input type="text" placeholder="Search entire store here...">
                            <div class="action">
                                <a href="#"><i class="zmdi zmdi-search"></i></a>
                            </div>
                        </div>
                    </form>
                    <div class="close__wrap">
                        <span>close</span>
                    </div>
                </div>
                <!-- End Search Popup -->
                <!-- Start Bradcaump area -->
                <div class="ht__bradcaump__area bg-image--4">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="bradcaump__inner text-center">
                                    <h2 class="bradcaump-title">Checkout</h2>
                                    <nav class="bradcaump-content">
                                        <a class="breadcrumb_item" href="index.html">Home</a>
                                        <span class="brd-separetor">/</span>
                                        <span class="breadcrumb_item active">Checkout</span>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Bradcaump area -->
                <!-- Start Checkout Area -->
                <section class="wn__checkout__area section-padding--lg bg__white">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 col-12">
                                <div class="customer_details">
                                    <form action="executeOrder.htm" method="post" onsubmit="return validate()" >
                                        <h3>Billing details</h3>
                                        <p>${message}</p>
                                    <div class="customar__field">
                                        <div class="input_box">
                                            <label>Full name <span>*</span></label>
                                            <input type="text" name="customerName" id="customerName" value="${fullName}">
                                        </div>
                                        <div class="input_box">
                                            <label>Shipping Address <span>*</span></label>
                                            <input type="text" name="address" id="address" value="${address}">
                                        </div>
                                        <div class="input_box">
                                            <label>Phone <span>*</span></label>
                                            <input type="text" name="phone" id="phone" value="${phone}">
                                        </div>
                                        <div class="input_box">
                                            <label>Email Address <span>*</span></label>
                                            <input type="text" name="email" id="email" value="${email}">
                                        </div>                                    
                                    </div>
                                    <label>Payment Method <span>*</span></label>
                                    <div class="create__account">
                                        <div class="wn__accountbox">
                                            <input class="input-checkbox" name="paymentMethod" value="Cash On Delivery" type="radio" id="cash" checked>
                                            <span>Cash On Delivery</span>
                                        </div>
                                    </div>
                                    <div class="customer_details mt--20">
                                        <div class="differt__address">
                                            <input name="paymentMethod" value="Direct Bank Transfer" type="radio" id="direct">
                                            <span>Direct Bank Transfer</span>
                                        </div>
                                        <div class="customar__field mt--40 hiden">
                                            <div class="input_box">
                                                <label>Card Numbers <span>*</span></label>
                                                <input type="text" name="cardNumber">
                                            </div>
                                            <div class="input_box">
                                                <label>Account Name <span>*</span></label>
                                                <input type="text" name="accountName">
                                            </div>                                    
                                        </div>
                                    </div>
                                    <div class="buttons-set">
                                        <button class="button" type="submit"><span>Order</span></button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-lg-6 col-12 md-mt-40 sm-mt-40">
                            <div class="wn__order__box">
                                <h3 class="onder__title">Your order</h3>
                                <table >
                                    <thead>
                                        <tr>
                                            <th style="text-align: center ; padding: 20px 15px 20px 10px">Product</th>
                                            <th style="text-align: center ; padding: 20px 15px 20px 10px">Quantity</th>
                                            <th style="text-align: center ; padding: 20px 15px 20px 10px">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${listCart}" var="cart">
                                            <tr>
                                                <td style="text-align: center ; padding: 20px 15px 20px 10px">${cart.product.productName} </td>
                                                <td style="text-align: center ; padding: 20px 15px 20px 10px">${cart.quantity}</td>
                                                <td style="text-align: center ; padding: 20px 15px 20px 10px">$<fmt:formatNumber  value="${cart.product.price*(100-cart.product.discount)*cart.quantity/100}"></fmt:formatNumber></td>
                                                </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>


                                <ul class="shipping__method" style="padding: 15px 18px 10px 65px;">
                                    <li>Cart Subtotal <span>$<fmt:formatNumber  value="${totalAmount}"></fmt:formatNumber></span></li>
                                        <li>Shipping Charges
                                            <ul>
                                                <li> 
                                                <c:if test="${totalAmount<20}">
                                                    <label>Fixed : $1.00</label>
                                                </c:if>
                                                <c:if test="${totalAmount>=20}">
                                                    <label>Free Ship</label>
                                                </c:if>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                                <ul class="total__amount" style="padding: 15px 18px 10px 65px;">
                                    <c:if test="${totalAmount<20}">
                                        <li>Order Total <span>$<fmt:formatNumber  value="${totalAmount+1}"></fmt:formatNumber></span></li>
                                        </c:if>
                                        <c:if test="${totalAmount>=20}">
                                        <li>Order Total <span>$<fmt:formatNumber  value="${totalAmount}"></fmt:formatNumber></span></li>
                                        </c:if>
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
            <!-- End Checkout Area -->
            <!-- Footer Area -->
            <jsp:include page="Footer.jsp"></jsp:include>
                <!-- //Footer Area -->

            </div>
            <!-- //Main wrapper -->

            <!-- JS Files -->

        <jsp:include page="Script.jsp"></jsp:include>
        <script>
            $('.wn__accountbox').on('click', function () {
                $('.account__field').slideToggle().remove('style');
            });
            $('.differt__address').on('click', function () {
                $('.differt__form').slideToggle().remove('style');
            });
            $(document).ready(function () {
                $(".hiden").hide();
                $('#direct').click(function () {
                    $(".hiden").show();
                });
                $('#cash').click(function () {
                    $(".hiden").hide();
                });
            });
        </script>
    </body>

    <!-- Mirrored from demo.devitems.com/boighor-v2/checkout.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 04 Apr 2019 10:36:56 GMT -->
</html>