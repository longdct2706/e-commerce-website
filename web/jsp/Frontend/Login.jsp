<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="no-js" lang="">

    <!-- Mirrored from demo.devitems.com/boighor-v2/shop-list.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 04 Apr 2019 10:36:44 GMT -->
    <jsp:include page="Head.jsp"></jsp:include>
    <script>
    function validate() {
        var username = document.getElementById("username").value;
        var pass = document.getElementById("pass").value;
        if (username != "" && pass !="") {
            return true;
        } else {
            if (username == "") {
                alert("You have to input Username");
                document.getElementById("username").focus();
                return false;
            }
            if (pass == "") {
                alert("You have to input Password");
                document.getElementById("pass").focus();
                return false;
            }
        }

    }
</script>
        <body>
            <!--[if lte IE 9]>
                    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
            <![endif]-->

            <!-- Main wrapper -->
            <div class="wrapper" id="wrapper">
                <!-- Header -->
            <jsp:include page="Header.jsp"></jsp:include>
                <!-- //Header -->
                <!-- Start Search Popup -->
                <div class="box-search-content search_active block-bg close__top">
                    <form id="search_mini_form" class="minisearch" action="#">
                        <div class="field__search">
                            <input type="text" placeholder="Search entire store here...">
                            <div class="action">
                                <a href="#"><i class="zmdi zmdi-search"></i></a>
                            </div>
                        </div>
                    </form>
                    <div class="close__wrap">
                        <span>close</span>
                    </div>
                </div>
                <!-- End Search Popup -->
                <!-- Start Bradcaump area -->
                <div class="ht__bradcaump__area bg-image--6">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="bradcaump__inner text-center">
                                    <h2 class="bradcaump-title">My Account</h2>
                                    <nav class="bradcaump-content">
                                        <a class="breadcrumb_item" href="index.html">Home</a>
                                        <span class="brd-separetor">/</span>
                                        <span class="breadcrumb_item active">My Account</span>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Bradcaump area -->
                <!-- Start My Account Area -->
                <section class="my_account_area pt--80 pb--55 bg--white">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-3"></div>
                            <div class="col-lg-6 col-12">
                                <div class="my__account__wrapper">
                                    <h3 class="account__title">Login</h3>
                                    <p>${message}</p>
                                    <form action="checkLogin.htm" method="post" onsubmit="return validate()">
                                        <div class="account__form">
                                            <div class="input__box">
                                                <label>Username<span>*</span></label>
                                                <input type="text" name="userName" id="username">
                                            </div>
                                            <div class="input__box">
                                                <label>Password<span>*</span></label>
                                                <input type="password" name="pass" id="pass">
                                            </div>
                                            <div class="form__btn">
                                                <button type="submit">Login</button>
                                                <label class="label-for-checkbox">
                                                    <input id="rememberme" class="input-checkbox" name="rememberme" value="forever" type="checkbox">
                                                    <span>Remember me</span>
                                                </label>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- End My Account Area -->
                <!-- Footer Area -->
            <jsp:include page="Footer.jsp"></jsp:include>
                <!-- //Footer Area -->

            </div>
            <!-- //Main wrapper -->

            <!-- JS Files -->
        <jsp:include page="Script.jsp"></jsp:include>

    </body>

    <!-- Mirrored from demo.devitems.com/boighor-v2/my-account.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 04 Apr 2019 10:36:58 GMT -->
</html>