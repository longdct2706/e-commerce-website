<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="no-js" lang="">

    <!-- Mirrored from demo.devitems.com/boighor-v2/shop-list.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 04 Apr 2019 10:36:44 GMT -->
                 
    <jsp:include page="Head.jsp"></jsp:include>
<link rel="stylesheet" href="/e-commerce-website/jsp/Frontend/css/bootstrap.min_1.css">  
        <style>
            .page-title {
                font-size: 36px;
                padding: 0 0 15px;
                margin: 30px 0 40px;
                position: relative;
                text-align: center;
                text-transform: uppercase; 
            }
            .page-title:after {
                background: #f25862;
                bottom: 0;
                content: "";
                display: block;
                height: 2px;
                left: 50%;
                margin-left: -30px;
                position: absolute;
                width: 60px; 
            }
            .page-title {
                font-size: 22px;
                margin-bottom: 15px; 
            }
            .container {
                padding-right: 15px;
                padding-left: 15px;
                margin-right: auto;
                margin-left: auto;
            }
            .form-inline {
                display: -ms-flexbox;
                display: block;
                -ms-flex-flow: row wrap;
                flex-flow: row wrap;
                -ms-flex-align: center;
                align-items: center;
            }
            .form-inline label {
                display: -ms-flexbox;
                display: block;
                -ms-flex-align: center;
                align-items: center;
                -ms-flex-pack: center;
                justify-content: center;
                margin-bottom: 5px;
            }
            th {
                font-weight: bold;
                text-transform: none; 
            }
            .other-page{font-family: 'Roboto', sans-serif;color:#666666}
            .other-page{
                padding-top: 10px;
            }
            label {
                color: #333333;
            }
            a:hover, a:focus {
                color: #e59285;
                text-decoration: none;
            }
            body{
                font-family: "Poppins", sans-serif;
            }
        </style>
        <body >
            <!--[if lte IE 9]>
                    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
            <![endif]-->
            <!-- Main wrapper -->
            <div class="wrapper" id="wrapper">
            <!-- Header -->
        <jsp:include page="Header.jsp"></jsp:include>
        </div>
            <!-- //Header -->
            <!-- Start Search Popup -->
            <!-- End Search Popup -->
            <!-- Start Bradcaump area -->
            <div class="ht__bradcaump__area bg-image--5">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="bradcaump__inner text-center">
                                <h2 class="bradcaump-title">Order History</h2>
                                <nav class="bradcaump-content">
                                    <a class="breadcrumb_item" href="index.html">Home</a>
                                    <span class="brd-separetor">/</span>
                                    <span class="breadcrumb_item active">Order History</span>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Bradcaump area -->
            <!-- Start Error Area -->
            <div class="other-page">
            <section class="content container">

                <!-- Default box -->
                <div class="box">
                    <h2 class="page-title">Order History</h2>
                    <div class="box-body">
                        <table id="product" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Order Id</th>
                                    <th>Customer Name</th>
                                    <th>Total Amount</th>
                                    <th>Payment Method</th>
                                    <th>Date</th>
                                    <th>Status</th>
                                    <th>Order Detail</th>
                                </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${listOrders}" var="order">
                                <tr>
                                    <td>${order.orderId}</td>
                                    <td>${order.customerName}</td>
                                    <td><fmt:formatNumber  value="${order.totalAmount}"></fmt:formatNumber></td>
                                    <td>${order.paymentMethod}</td>
                                    <td>${order.createdDate}</td>
                                    <c:if test="${order.status==true}">
                                        <td>Delivered</td>
                                    </c:if>
                                    <c:if test="${order.status==false}">
                                        <td>Not Delivery</td>
                                    </c:if>                       
                                    <td>
                                        <a href="getOrderDetailByOrderId.htm?orderId=${order.orderId}">Detail</a>
                                    </td>
                                </c:forEach>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Order Id</th>
                                <th>Customer Name</th>
                                <th>Total Amount</th>
                                <th>Payment Method</th>
                                <th>Date</th>
                                <th>Status</th>
                                <th>Order Detail</th>               
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </section>
            </div>
        <!-- End Error Area -->

        <!-- Footer Area -->
        <jsp:include page="Footer.jsp"></jsp:include>
            <!-- //Footer Area -->
            <!-- //Main wrapper -->
            <!-- JS Files -->
        <jsp:include page="Script.jsp"></jsp:include>
        <!-- bootstrap js -->
        <!-- owl.carousel js -->
        <!-- main js -->
        <script src="/ShoesDemo/jsp/Frontend/js/main.js"></script>
        <script src="/e-commerce-website/jsp/Admin/js/jquery.min.js"></script>
        <script src="/e-commerce-website/jsp/Admin/js/jquery-ui.js"></script>
        <script src="/e-commerce-website/jsp/Admin/js/jquery.dataTables.min.js"></script>
        <script src="/e-commerce-website/jsp/Admin/js/dataTables.bootstrap.min.js"></script>
        <script src="/e-commerce-website/jsp/Admin/js/bootstrap.min.js"></script>
        <script src="/e-commerce-website/jsp/Admin/js/adminlte.min.js"></script>
        <script src="/e-commerce-website/jsp/Admin/js/dashboard.js"></script>
        <script src="/e-commerce-website/jsp/Admin/js/function.js"></script>
        <script>
            $(function () {
                $('#product').DataTable({
                    'paging': true,
                    'lengthChange': true,
                    'searching': true,
                    'ordering': true,
                    'info': true,
                    'autoWidth': false
                })
            })
        </script>
    </body>

    <!-- Mirrored from demo.devitems.com/boighor-v2/error404.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 04 Apr 2019 10:38:32 GMT -->
</html>