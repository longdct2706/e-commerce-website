<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="no-js" lang="">

    <!-- Mirrored from demo.devitems.com/boighor-v2/shop-list.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 04 Apr 2019 10:36:44 GMT -->
    <jsp:include page="Head.jsp"></jsp:include>
        <script>
            function validate() {
                var fullname = document.getElementById("fullname").value;
                var username = document.getElementById("username").value;
                var pass = document.getElementById("pass").value;
                var confirmpass = document.getElementById("confirmpass").value;
                var compare = pass.localeCompare(confirmpass);
                var email = document.getElementById("email").value;
                var address = document.getElementById("address").value;
                var phone = document.getElementById("phone").value;
                var check = true;
                var i;
                for (i = 0; i < phone.length; i++) {
                    if (phone.charCodeAt(0) != 48) {
                        check = false;
                        break;
                    }
                    if (phone.charCodeAt(i) < 48 || phone.charCodeAt(i) > 57) {
                        check = false;
                        break;
                    }
                }
                if (fullname != "" && username != "" && pass != "" && confirmpass != "" && compare == 0 && email != "" && address != "" && phone != "" && check == true && phone.length == 10) {
                    return true;
                } else {
                    if (fullname == "") {
                        alert("You have to input Fullname");
                        document.getElementById("fullname").focus();
                        return false;
                    }
                    if (username == "") {
                        alert("You have to input Username");
                        document.getElementById("username").focus();
                        return false;
                    }
                    if (pass == "") {
                        alert("You have to input Password");
                        document.getElementById("pass").focus();
                        return false;
                    }
                    if (confirmpass == "") {
                        alert("You have to input Confirm Password");
                        document.getElementById("confirmpass").focus();
                        return false;
                    }
                    if (compare != 0) {
                        alert("Your Confirm Password is incorrect");
                        document.getElementById("confirmpass").focus();
                        return false;
                    }
                    if (email == "") {
                        alert("You have to input Email");
                        document.getElementById("email").focus();
                        return false;
                    }
                    if (address == "") {
                        alert("You have to input Address");
                        document.getElementById("address").focus();
                        return false;
                    }
                    if (phone == "") {
                        alert("You have to input Phone");
                        document.getElementById("phone").focus();
                        return false;
                    }
                    if (check != true) {
                        alert("You have to input numbers from 0-9 and start from 0");
                        document.getElementById("phone").focus();
                        return false;
                    }
                    if (phone.length != 10) {
                        alert("Phone must have 10 numbers");
                        document.getElementById("phone").focus();
                        return false;
                    }
                }

            }
        </script>
        <style>
            .message{
                color: #E50D0D;
            }
        </style>
        <body>
            <!--[if lte IE 9]>
                    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
            <![endif]-->

            <!-- Main wrapper -->
            <div class="wrapper" id="wrapper">
                <!-- Header -->
            <jsp:include page="Header.jsp"></jsp:include>
                <!-- //Header -->
                <!-- Start Search Popup -->
                <div class="box-search-content search_active block-bg close__top">
                    <form id="search_mini_form" class="minisearch" action="#">
                        <div class="field__search">
                            <input type="text" placeholder="Search entire store here...">
                            <div class="action">
                                <a href="#"><i class="zmdi zmdi-search"></i></a>
                            </div>
                        </div>
                    </form>
                    <div class="close__wrap">
                        <span>close</span>
                    </div>
                </div>
                <!-- End Search Popup -->
                <!-- Start Bradcaump area -->
                <div class="ht__bradcaump__area bg-image--6">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="bradcaump__inner text-center">
                                    <h2 class="bradcaump-title">My Account</h2>
                                    <nav class="bradcaump-content">
                                        <a class="breadcrumb_item" href="index.html">Home</a>
                                        <span class="brd-separetor">/</span>
                                        <span class="breadcrumb_item active">My Account</span>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Bradcaump area -->
                <!-- Start My Account Area -->
                <section class="my_account_area pt--80 pb--55 bg--white">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-3"></div>
                            <div class="col-lg-6 col-12">
                                <div class="my__account__wrapper">
                                    <h3 class="account__title">Register</h3>
                                    <p class="message">${message}</p>
                                <p>${message1}</p>
                                <f:form action="registerUser.htm" method="post" commandName="users" onsubmit="return validate()">
                                    <div class="account__form">
                                        <div class="input__box">
                                            <label>Full Name<span>*</span></label>
                                            <f:input type="text" path="fullName" id="fullname"/>
                                        </div>                                            
                                        <div class="input__box">
                                            <label>Username<span>*</span></label>
                                            <f:input type="text" path="userName" id="username"/>
                                        </div>
                                        <div class="input__box">
                                            <label>Password<span>*</span></label>
                                            <f:input type="password" path="pass" id="pass"/>
                                        </div>
                                        <div class="input__box">
                                            <label>Confirm Password<span>*</span></label>
                                            <input type="password" id="confirmpass"/>
                                        </div>                                        
                                        <div class="input__box">
                                            <label>Email<span>*</span></label>
                                            <f:input type="email" path="email" id="email"/>
                                        </div>
                                        <div class="input__box">
                                            <label>Address<span>*</span></label>
                                            <f:input type="text" path="address" id="address"/>
                                        </div>  
                                        <div class="input__box">
                                            <label>Phone<span>*</span></label>
                                            <f:input type="text" path="phone" id="phone"/>
                                        </div>                                             
                                        <div class="form__btn">
                                            <button type="submit">Register</button>
                                        </div>
                                    </div>
                                </f:form>>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End My Account Area -->
            <!-- Footer Area -->
            <jsp:include page="Footer.jsp"></jsp:include>
                <!-- //Footer Area -->

            </div>
            <!-- //Main wrapper -->

            <!-- JS Files -->
        <jsp:include page="Script.jsp"></jsp:include>

    </body>

    <!-- Mirrored from demo.devitems.com/boighor-v2/my-account.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 04 Apr 2019 10:36:58 GMT -->
</html>