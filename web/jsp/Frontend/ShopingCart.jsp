<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="no-js" lang="">

    <!-- Mirrored from demo.devitems.com/boighor-v2/shop-list.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 04 Apr 2019 10:36:44 GMT -->
    <jsp:include page="Head.jsp"></jsp:include>
        <style>
            #updatecart{
                color: #333;
                background: #fff none repeat scroll 0 0;
                border: 0px solid;
                border-radius: 5px;
                display: block;
                font-family: "Poppins", sans-serif;
                font-size: 16px;
                font-weight: 400;
                height: 50px;
                line-height: 50px;
                padding: 0 26px;
                text-transform: capitalize;
                transition: all 0.3s ease 0s;
            }
            #updatecart:hover{
                background: #e59285 none repeat scroll 0 0;
                color: #fff;
            }
            img {
                max-width: 75%;
            }
            .shopingcart-bottom-area a:hover, .discount-middle > a:hover, .subtotal-main-area > a:hover, .table-bottom-area .bottom-button > a:hover {
                background: #FF7575 none repeat scroll 0 0;
                border-color: #FF7575;
                color: #fff;
            }
            .table-bottom-area .bottom-button > a, .shopingcart-bottom-area a, .discount-middle > a,.bottom-button >input {
                background: #FFFFFF none repeat scroll 0 0;
                border: 1px solid #666666;
                border-radius: 20px;
                color: #666666;
                font-weight: 500;
                margin-left: 5px;
                padding: 10px 25px;

            }
            .shopingcart-bottom-area .bottom-button {
                float: right;
            }
            .content-empty, .content-empty .image-square {
                margin-bottom: 1.2rem;
                text-align: center;
            }
            .cartEmpty_2MjW .title_1x2N {
                display: block;
                margin-bottom: 3.2rem;
                font-size: 1.6rem;
            }
            .img {
                vertical-align: middle;
                border-style: none;
                width: 200px;
                height: 200px;
            }
            .bottoma{
                margin: 0px 0px 0px 5%;
                margin-left: 0% !important;
                padding: 10px 60px !important;
            }
            strong {
                font-weight: 500;
                font-family: Roboto,Helvetica Neue,Arial,sans-serif;
                display: block;
                margin-bottom: 30px;
                font-size: 16px;
                color: #666666;
            }
        </style>
        <body>
            <!--[if lte IE 9]>
                    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
            <![endif]-->

            <!-- Main wrapper -->
            <div class="wrapper" id="wrapper">

                <!-- Header -->
            <jsp:include page="Header.jsp"></jsp:include>
                <!-- //Header -->
                <!-- Start Search Popup -->
                <div class="box-search-content search_active block-bg close__top">
                    <form id="search_mini_form" class="minisearch" action="#">
                        <div class="field__search">
                            <input type="text" placeholder="Search entire store here...">
                            <div class="action">
                                <a href="#"><i class="zmdi zmdi-search"></i></a>
                            </div>
                        </div>
                    </form>
                    <div class="close__wrap">
                        <span>close</span>
                    </div>
                </div>
                <!-- End Search Popup -->
                <!-- Start Bradcaump area -->
                <div class="ht__bradcaump__area bg-image--3">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="bradcaump__inner text-center">
                                    <h2 class="bradcaump-title">Shopping Cart</h2>
                                    <nav class="bradcaump-content">
                                        <a class="breadcrumb_item" href="index.html">Home</a>
                                        <span class="brd-separetor">/</span>
                                        <span class="breadcrumb_item active">Shopping Cart</span>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Bradcaump area -->
                <!-- cart-main-area start -->
                <div class="cart-main-area section-padding--lg bg--white">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 ol-lg-12">
                            <c:if test="${listCart!=null && listCart.size()!=0}">
                                <div class="table-content wnro__table table-responsive">
                                    <table>
                                        <thead>
                                            <tr class="title-top">
                                                <th class="product-thumbnail">Image</th>
                                                <th class="product-name">Product</th>
                                                <th class="product-price">Price</th>
                                                <th class="product-quantity">Quantity</th>
                                                <th class="product-subtotal">Total</th>
                                                <th class="product-remove">Remove</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <form action="updateQuantity.htm">
                                            <c:forEach items="${listCart}" var="cart">
                                                <tr>
                                                    <td class="product-thumbnail"><a href="initSingleProduct.htm?productId=${cart.product.productId}"><img src="/e-commerce-website/jsp/images/${cart.product.images}" alt="product img"></a></td>
                                                    <td class="product-name"><a href="initSingleProduct.htm?productId=${cart.product.productId}">${cart.product.productName}</a></td>
                                                    <td class="product-price"><span class="amount">$<fmt:formatNumber  value="${cart.product.price*(100-cart.product.discount)/100}"></fmt:formatNumber></span></td>
                                                        <td class="product-quantity">
                                                            <input type="number" value="${cart.quantity}"  name="quantity" min="1"><br>
                                                        <p>${listMessage.get(listCart.indexOf(cart))}</p>
                                                    </td>
                                                    <td class="product-subtotal">$<fmt:formatNumber  value="${cart.product.price*(100-cart.product.discount)*cart.quantity/100}"></fmt:formatNumber></td>
                                                    <td class="product-remove"><a href="removeCart.htm?productId=${cart.product.productId}">X</a></td>
                                                </tr>
                                            </c:forEach>
                                            </tbody>
                                    </table>
                                </div>
                                <div class="cartbox__btn">
                                    <ul class="cart__btn__list d-flex flex-wrap flex-md-nowrap flex-lg-nowrap justify-content-between">
                                        <li><a href="getAllProduct.htm">Continue Shopping</a></li>
                                        <li><a href="removeAllCart.htm" >Clear Shopping Cart</a></li>
                                        <li><input type="submit" value="Update Cart" id="updatecart"></li>
                                        <li><a href="initCheckOut.htm">Check Out</a></li>
                                    </ul>
                                </div>
                                </form>
                            </c:if>
                            <c:if test="${listCart==null||listCart.size()==0}">
                                <div class="shopingcart-bottom-area content-empty ">
                                    <div class="image-square">
                                        <img class="img" src="//pwa.scdn.vn/static/media/cart-empty.e2664e0f.svg" alt="Giỏ hàng đang trống">
                                    </div>
                                    <strong class="title_1x2N">Cart is empty</strong>
                                    <a href="<%=request.getContextPath()%>/UserController/getAllProduct.htm" class="bottoma">Continue Shopping</a>
                                </div>
                            </c:if>                                        
                        </div>
                    </div>
                    <c:if test="${listCart!=null && listCart.size()!=0}">
                        <div class="row">
                            <div class="col-lg-6 offset-lg-6">
                                <div class="cartbox__total__area">
                                    <div class="cartbox-total d-flex justify-content-between">
                                        <ul class="cart__total__list">
                                            <li>Cart total</li>
                                            <li>Sub Total</li>
                                        </ul>
                                        <ul class="cart__total__tk">
                                            <li>$<fmt:formatNumber  value="${totalAmount}"></fmt:formatNumber></li>
                                            <li>$<fmt:formatNumber  value="${totalAmount}"></fmt:formatNumber></li>
                                            </ul>
                                        </div>
                                        <div class="cart__total__amount">
                                            <span>Grand Total</span>
                                            <span>$<fmt:formatNumber  value="${totalAmount}"></fmt:formatNumber></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </c:if>
                </div>  
            </div>
            <!-- cart-main-area end -->
            <!-- Footer Area -->
            <jsp:include page="Footer.jsp"></jsp:include>
                <!-- //Footer Area -->

            </div>
            <!-- //Main wrapper -->

            <!-- JS Files -->
        <jsp:include page="Script.jsp"></jsp:include>

    </body>

    <!-- Mirrored from demo.devitems.com/boighor-v2/cart.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 04 Apr 2019 10:36:56 GMT -->
</html>