<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="no-js" lang="">

    <!-- Mirrored from demo.devitems.com/boighor-v2/shop-list.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 04 Apr 2019 10:36:44 GMT -->
    <jsp:include page="Head.jsp"></jsp:include>
        <script>
            function validate() {
                var minprice = document.getElementById("minprice").value;
                var maxprice = document.getElementById("maxprice").value;
                if (minprice != '' && maxprice != '') {
                    return true;
                } else {
                    if (minprice == '') {
                        alert("You have to input Min Price");
                        document.getElementById("minprice").focus();
                        return false;
                    }
                    if (maxprice == '') {
                        alert("You have to input Max Price");
                        document.getElementById("maxprice").focus();
                        return false;
                    }
                }
            }
        </script>
        <style>
            #page{
                background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
                border-bottom: 2px solid transparent;
                color: #777;
                display: inline-block;
                font-size: 14px;
                font-weight: 600;
                line-height: 20px;
                margin: 0 5px;
                padding: 0 13px;
                text-align: center;
                color: #333;
            }
            #page:hover{
                border-color: #363636;
                color: #363636;
            }
            .containing{
                margin-bottom: 25px;
                text-align: center;
            }
            .pagination{
                display: inline-flex;
            }
            #activate{
                background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
                border-bottom: 2px solid transparent;
                color: #777;
                display: inline-block;
                font-size: 14px;
                font-weight: 600;
                line-height: 20px;
                margin: 0 5px;
                padding: 0 13px;
                text-align: center;
                color: #333;
                border-color: #363636;
                color: #363636;
            }
            .price_filter >form  > label{
                padding-top: 7px;
                margin-bottom: 0;
                text-align: right;
                font-family: 'Roboto', sans-serif;
                margin-bottom: 15px;
                color: #333333;
                margin-bottom: 5px;
                font-weight: bold;
            }
            .price_filter >form > div > input {
                font-family: 'Roboto', sans-serif;
                display: block;
                width: 100%;
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857143;
                color: #555;
                background-color: #fff;
                background-image: none;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-shadow: inset 0 1px 1px rgba(0,0,0,.075);                
                transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
                transition-property: border-color, box-shadow;
                transition-duration: 0.15s, 0.15s;
                transition-timing-function: ease-in-out, ease-in-out;
                transition-delay: 0s, 0s;
                margin-bottom: 15px;
            }
            .price_filter >form>div>input[type="submit"]:hover{
                background: #FF7575;
                color: #fff;
                border-color: #FF7575;
            }
            .price_filter >form>div>input[type="submit"]{
                border: 1px solid #666666;
                color: #666666;
                float: left;
                font-weight: 500;
                height: 40px;
                letter-spacing: 1px;
                text-transform: uppercase;
                transition: all 400ms ease-out 0s;
                width: 85px;
            }
            .product-info {
                padding-left: 50px;
                max-width: 75%;
            }
        </style>
        <body>
            <!--[if lte IE 9]>
                    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
            <![endif]-->

            <!-- Main wrapper -->
            <div class="wrapper" id="wrapper">

                <!-- Header -->
            <jsp:include page="Header.jsp"></jsp:include>
                <!-- //Header -->
                <!-- Start Search Popup -->
                <!-- End Search Popup -->
                <!-- Start Bradcaump area -->
                <div class="ht__bradcaump__area bg-image--5">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="bradcaump__inner text-center">
                                    <h2 class="bradcaump-title">Shop List</h2>
                                    <nav class="bradcaump-content">
                                        <a class="breadcrumb_item" href="index.html">Home</a>
                                        <span class="brd-separetor">/</span>
                                        <span class="breadcrumb_item active">Shop List</span>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Bradcaump area -->
                <!-- Start Shop Page -->
                <div class="page-shop-sidebar left--sidebar bg--white section-padding--lg">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-3 col-12 order-2 order-lg-1 md-mt-40 sm-mt-40">
                                <div class="shop__sidebar">
                                    <aside class="wedget__categories poroduct--cat">
                                        <h3 class="wedget__title">Product Categories</h3>
                                        <ul>
                                        <c:forEach items="${listCatalog}" var="catalog">
                                            <li><a href="getProductByCatalogId.htm?catalogId=${catalog.catalogId}">${catalog.catalogName}</a></li>
                                            </c:forEach>
                                    </ul>
                                </aside>
                                <aside class="wedget__categories pro--range">
                                    <h3 class="wedget__title">Filter by price</h3>
                                    <div class="content-shopby">
                                        <div class="price_filter s-filter clear ">
                                            <c:if test="${catalogId==null}">
                                                <form action="filterPrice.htm" onsubmit="return validate()" >
                                                    <label class="col-lg-2" style="float:left">From</label>
                                                    <c:if test="${priceMin!=null}">
                                                        <div class="col-lg-10"style="float:left">
                                                            <input type="number" id="minprice" name="priceMin"  placeholder="Min Price" min="0"  value="${priceMin}" />
                                                        </div>
                                                    </c:if>
                                                    <c:if test="${priceMin==null}">
                                                        <div class="col-lg-10"style="float:left">
                                                            <input type="number" id="minprice" name="priceMin"  placeholder="Min Price" min="0"  value="5" />
                                                        </div>
                                                    </c:if>
                                                    <label class="col-lg-2" style="float:left">To</label>
                                                    <c:if test="${priceMax!=null}">
                                                        <div class="col-lg-10"style="float:left">
                                                            <input type="number" id="maxprice" name="priceMax"  placeholder="Max Price" min="0"  value="${priceMax}"/> 
                                                        </div>
                                                    </c:if>
                                                    <c:if test="${priceMax==null}">
                                                        <div class="col-lg-10"style="float:left">
                                                            <input type="number" id="maxprice" name="priceMax"  placeholder="Max Price" min="0"  value="10"/> 
                                                        </div>
                                                    </c:if>
                                                    <div class="col-lg-3"style="float:left"></div>
                                                    <div class="col-lg-9 price_slider_amount"style="float:left">                                                  
                                                        <input type="submit"  value="Filter"/>  
                                                    </div>
                                                </form>
                                            </c:if>
                                            <c:if test="${catalogId!=null}">
                                                <form action="filterPriceCatalog.htm" onsubmit="return validate()" >
                                                    <label class="col-lg-2" style="float:left">From</label>
                                                    <input type="hidden" name="catalogId" value="${catalogId}"/>
                                                    <c:if test="${priceMin!=null}">
                                                        <div class="col-lg-10"style="float:left">
                                                            <input type="number" id="minprice" name="priceMin"  placeholder="Min Price" min="0"  value="${priceMin}" />
                                                        </div>
                                                    </c:if>
                                                    <c:if test="${priceMin==null}">
                                                        <div class="col-lg-10"style="float:left">
                                                            <input type="number" id="minprice" name="priceMin"  placeholder="Min Price" min="0"  value="5" />
                                                        </div>
                                                    </c:if>
                                                    <label class="col-lg-2" style="float:left">To</label>
                                                    <c:if test="${priceMax!=null}">
                                                        <div class="col-lg-10"style="float:left">
                                                            <input type="number" id="maxprice" name="priceMax"  placeholder="Max Price" min="0"  value="${priceMax}"/> 
                                                        </div>
                                                    </c:if>
                                                    <c:if test="${priceMax==null}">
                                                        <div class="col-lg-10"style="float:left">
                                                            <input type="number" id="maxprice" name="priceMax"  placeholder="Max Price" min="0"  value="10"/> 
                                                        </div>
                                                    </c:if>
                                                    <div class="col-lg-3"style="float:left"></div>
                                                    <div class="col-lg-9 price_slider_amount"style="float:left">                                                  
                                                        <input type="submit"  value="Filter"/>  
                                                    </div>
                                                </form>
                                            </c:if>                                            
                                        </div>
                                    </div>
                                </aside>
                            </div>
                        </div>
                        <div class="col-lg-9 col-12 order-1 order-lg-2">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="shop__list__wrapper d-flex flex-wrap flex-md-nowrap justify-content-between">
                                        <div class="shop__list nav justify-content-center" role="tablist">
                                            <a class="nav-item nav-link" data-toggle="tab" href="#nav-grid" role="tab"><i class="fa fa-th"></i></a>
                                            <a class="nav-item nav-link active" data-toggle="tab" href="#nav-list" role="tab"><i class="fa fa-list"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab__container">
                                <div class="shop-grid tab-pane fade" id="nav-grid" role="tabpanel">
                                    <div class="paging containing "></div>  
                                    <div class="row">
                                        <!-- Start Single Product -->
                                        <c:forEach items="${listProduct}" var="pro">
                                            <div class="product product__style--3 col-lg-4 col-md-4 col-sm-6 col-12">
                                                <div class="product__thumb">
                                                    <a class="first__img" href="initSingleProduct.htm?productId=${pro.productId}"><img src="/e-commerce-website/jsp/images/${pro.images}" alt="product image" width="230px" height="300px"></a>
                                                </div>
                                                <div class="product__content content--center">
                                                    <h4><a href="initSingleProduct.htm?productId=${pro.productId}">${pro.productName}</a></h4>
                                                    <ul class="prize d-flex">
                                                        <li>$<fmt:formatNumber  value="${pro.price*(100-pro.discount)/100}"></fmt:formatNumber></li>
                                                            <c:if test="${pro.discount!=0}">
                                                            <li class="old_prize">$<fmt:formatNumber  value="${pro.price}"></fmt:formatNumber></li>
                                                            </c:if>
                                                        </ul>
                                                        <div class="action">
                                                            <div class="actions_inner">
                                                                <ul class="add_to_links">
                                                                    <li><a class="cart" title="Product Detail" href="initSingleProduct.htm?productId=${pro.productId}"><i class="bi bi-shopping-bag4"></i></a></li>
                                                                <li><a class="wishlist" title="Add Cart" href="initAddCart1.htm?productId=${pro.productId}"><i class="bi bi-shopping-cart-full"></i></a></li>
                                                                <li><a data-toggle="modal" title="Quick View" class="quickview modal-view detail-link" href="#${pro.productId}"><i class="bi bi-search"></i></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="product__hover--content">
                                                        <ul class="rating d-flex">
                                                            <li class="on"><i class="fa fa-star-o"></i></li>
                                                            <li class="on"><i class="fa fa-star-o"></i></li>
                                                            <li class="on"><i class="fa fa-star-o"></i></li>
                                                            <li><i class="fa fa-star-o"></i></li>
                                                            <li><i class="fa fa-star-o"></i></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="quickview-wrapper">
                                                <!-- Modal -->
                                                <div class="modal fade" id="${pro.productId}" tabindex="-1" role="dialog">
                                                    <div class="modal-dialog modal__container" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header modal__header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="modal-product">
                                                                    <!-- Start product images -->
                                                                    <div class="product-images">
                                                                        <div class="main-image images">
                                                                            <img alt="big images" src="/e-commerce-website/jsp/images/${pro.images}">
                                                                        </div>
                                                                    </div>
                                                                    <!-- end product images -->
                                                                    <div class="product-info">
                                                                        <h1>${pro.productName}</h1>
                                                                        <div class="rating__and__review">
                                                                            <ul class="rating">
                                                                                <li><span class="ti-star"></span></li>
                                                                                <li><span class="ti-star"></span></li>
                                                                                <li><span class="ti-star"></span></li>
                                                                                <li><span class="ti-star"></span></li>
                                                                                <li><span class="ti-star"></span></li>
                                                                            </ul>

                                                                        </div>
                                                                        <div class="price-box-3">
                                                                            <div class="s-price-box">
                                                                                <span class="new-price">$<fmt:formatNumber  value="${pro.price*(100-pro.discount)/100}"></fmt:formatNumber></span>
                                                                                <c:if test="${pro.discount!=0}">
                                                                                <span class="old-price">$<fmt:formatNumber  value="${pro.price}"></fmt:formatNumber></span>
                                                                                </c:if>
                                                                                </div>
                                                                            </div>
                                                                            <div class="quick-desc">
                                                                            ${pro.productContent}
                                                                        </div>
                                                                        <div class="addtocart-btn">
                                                                            <a href="initAddCart1.htm?productId=${pro.productId}">Add to cart</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </c:forEach>
                                        <!-- End Single Product -->
                                    </div>
                                    <div class="paging containing "></div>  
                                </div>
                                <div class="shop-grid tab-pane fade show active" id="nav-list" role="tabpanel">
                                    <div class="paging12 containing "></div>  
                                    <div class="list__view__wrapper">
                                        <!-- Start Single Product -->
                                        <c:forEach items="${listProduct}" var="pro">
                                            <div class="list__view">
                                                <div class="thumb">
                                                    <a class="first__img" href="initSingleProduct.htm?productId=${pro.productId}"><img src="/e-commerce-website/jsp/images/${pro.images}" alt="product image" width="230px" height="300px" style="margin-bottom: 15px"></a>
                                                </div>
                                                <div class="content">
                                                    <h2><a href="initSingleProduct.htm?productId=${pro.productId}">${pro.productName}</a></h2>
                                                    <ul class="rating d-flex">
                                                        <li class="on"><i class="fa fa-star-o"></i></li>
                                                        <li class="on"><i class="fa fa-star-o"></i></li>
                                                        <li class="on"><i class="fa fa-star-o"></i></li>
                                                        <li class="on"><i class="fa fa-star-o"></i></li>
                                                        <li><i class="fa fa-star-o"></i></li>
                                                        <li><i class="fa fa-star-o"></i></li>
                                                    </ul>
                                                    <ul class="prize__box">
                                                        <li>$<fmt:formatNumber  value="${pro.price*(100-pro.discount)/100}"></fmt:formatNumber></li>
                                                        <c:if test="${pro.discount!=0}">
                                                        <li class="old__prize">$<fmt:formatNumber  value="${pro.price}"></fmt:formatNumber></li>
                                                        </c:if>
                                                        </ul>
                                                        <ul class="cart__action d-flex">
                                                            <li class="cart"><a href="initAddCart1.htm?productId=${pro.productId}">Add to cart</a></li>
                                                    </ul>

                                                </div>
                                            </div>

                                        </c:forEach>
                                        <!-- End Single Product -->
                                    </div>
                                    <div class="paging12 containing "></div>  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Shop Page -->
            <!-- Footer Area -->
            <jsp:include page="Footer.jsp"></jsp:include>
                <!-- //Footer Area -->
                <!-- QUICKVIEW PRODUCT -->
                <!-- END QUICKVIEW PRODUCT -->

            </div>
            <!-- //Main wrapper -->

            <!-- JS Files -->
        <jsp:include page="Script.jsp"></jsp:include>
        <script type="text/javascript" language="javascript" src="/e-commerce-website/jsp/Frontend/js/paginate12.js">
        </script>
        <script type="text/javascript" language="javascript" src="/e-commerce-website/jsp/Frontend/js/custom12.js">
        </script>
        <script type="text/javascript" language="javascript" src="/e-commerce-website/jsp/Frontend/js/paginate.js">
        </script>
        <script type="text/javascript" language="javascript" src="/e-commerce-website/jsp/Frontend/js/custom.js">
        </script>
    </body>

    <!-- Mirrored from demo.devitems.com/boighor-v2/shop-list.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 04 Apr 2019 10:36:44 GMT -->
</html>