<%-- 
    Document   : Head
    Created on : Apr 25, 2019, 9:52:12 AM
    Author     : H
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Shop-List | Bookshop Responsive Bootstrap4 Template</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Favicons -->
        <link rel="shortcut icon" href="/e-commerce-website/jsp/Frontend/images/favicon.ico">
        <link rel="apple-touch-icon" href="/e-commerce-website/jsp/Frontend/images/icon.png">

        <!-- Google font (font-family: 'Roboto', sans-serif; Poppins ; Satisfy) -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet"> 
        <link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,600,600i,700,700i,800" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">

        <!-- Stylesheets -->
         <link rel="stylesheet" href="/e-commerce-website/jsp/Frontend/css/bootstrap.min.css">                    
        <link rel="stylesheet" href="/e-commerce-website/jsp/Frontend/css/plugins.css">
        <link rel="stylesheet" href="/e-commerce-website/jsp/Frontend/css/style.css">

        <!-- Cusom css -->
        <link rel="stylesheet" href="/e-commerce-website/jsp/Frontend/css/custom.css">

        <!-- Modernizer js -->
        <script src="/e-commerce-website/jsp/Frontend/js/vendor/modernizr-3.5.0.min.js"></script>
    </head>
