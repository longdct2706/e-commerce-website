<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="no-js" lang="">

    <!-- Mirrored from demo.devitems.com/boighor-v2/shop-list.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 04 Apr 2019 10:36:44 GMT -->
    <jsp:include page="Head.jsp"></jsp:include>
        <style>
            .product-info {
                padding-left: 50px;
                max-width: 78%; }
            .product-images {
                max-width: 50%;
                padding-right: 0px;
            }
            .product__nav a {
                margin-top: 10px;
            }
            .owl-carousel .owl-item img {
                width: 92%;
            }
        </style>
        <body>
            <!--[if lte IE 9]>
                    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
            <![endif]-->

            <!-- Main wrapper -->
            <div class="wrapper" id="wrapper">
                <!-- Header -->
            <jsp:include page="Header.jsp"></jsp:include>
                <!-- //Header -->
                <!-- Start Search Popup -->
                <!-- End Search Popup -->
                <!-- Start Slider area -->
                <div class="slider-area brown__nav slider--15 slide__activation slide__arrow01 owl-carousel owl-theme">
                    <!-- Start Single Slide -->
                    <div class="slide animation__style10 bg-image--1 fullscreen align__center--left">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="slider__content">
                                        <div class="contentbox">
                                            <h2>Buy <span>your </span></h2>
                                            <h2>favourite <span>Book </span></h2>
                                            <h2>from <span>Here </span></h2>
                                            <a class="shopbtn" href="getAllProductShopList.htm">shop now</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Single Slide -->
                    <!-- Start Single Slide -->
                    <div class="slide animation__style10 bg-image--7 fullscreen align__center--left">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="slider__content">
                                        <div class="contentbox">
                                            <h2>Buy <span>your </span></h2>
                                            <h2>favourite <span>Book </span></h2>
                                            <h2>from <span>Here </span></h2>
                                            <a class="shopbtn" href="getAllProductShopList.htm">shop now</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Single Slide -->
                </div>
                <!-- End Slider area -->
                <!-- Start BEst Seller Area -->
                <section class="wn__product__area brown--color pt--80  pb--30">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="section__title text-center">
                                    <h2 class="title__be--2">Best <span class="color--theme">Seller</span></h2>
                                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered lebmid alteration in some ledmid form</p>
                                </div>
                            </div>
                        </div>
                        <!-- Start Single Tab Content -->
                        <div class="furniture--4 border--round arrows_style owl-carousel owl-theme row mt--50">
                            <!-- Start Single Product -->
                        <c:forEach items="${listBestProduct}" var="pro">
                            <div class="product product__style--3">
                                <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                                    <div class="product__thumb">
                                        <a class="first__img" href="initSingleProduct.htm"><img src="/e-commerce-website/jsp/images/${pro.images}" height="300px" alt="product image"></a>
                                        <div class="hot__box">
                                            <span class="hot-label">BEST SELLER</span>
                                        </div>
                                    </div>
                                    <div class="product__content content--center">
                                        <h4><a href="initSingleProduct.htm">${pro.productName}</a></h4>
                                        <ul class="prize d-flex">
                                            <li>$<fmt:formatNumber  value="${pro.price*(100-pro.discount)/100}"></fmt:formatNumber></li>
                                            <c:if test="${pro.discount!=0}">
                                            <li class="old_prize">$<fmt:formatNumber  value="${pro.price}"></fmt:formatNumber></li>
                                             </c:if>
                                        </ul>
                                        <div class="action">
                                            <div class="actions_inner">
                                                <ul class="add_to_links">
                                                    <li><a class="wishlist" title="Add Cart" href="initAddCart1.htm?productId=${pro.productId}"><i class="bi bi-shopping-cart-full"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="product__hover--content">
                                            <ul class="rating d-flex">
                                                <li class="on"><i class="fa fa-star-o"></i></li>
                                                <li class="on"><i class="fa fa-star-o"></i></li>
                                                <li class="on"><i class="fa fa-star-o"></i></li>
                                                <li><i class="fa fa-star-o"></i></li>
                                                <li><i class="fa fa-star-o"></i></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                    <!-- End Single Tab Content -->
                </div>
            </section>
            <!-- Start BEst Seller Area -->
            <!-- Start NEwsletter Area -->

            <!-- End NEwsletter Area -->
            <!-- Start Best Seller Area -->
            <section class="wn__bestseller__area bg--white pt--80  pb--30">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="section__title text-center">
                                <h2 class="title__be--2">All <span class="color--theme">Products</span></h2>
                                <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered lebmid alteration in some ledmid form</p>
                            </div>
                        </div>
                    </div>
                    <div class="row mt--50">
                        <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="product__nav nav justify-content-center" role="tablist">
                                <a class="nav-item nav-link active" data-toggle="tab" href="#nav-all" role="tab">ALL</a>
                                <c:forEach items="${listCatalog}" var="cat">
                                    <a class="nav-item nav-link" data-toggle="tab" href="#${cat.catalogId}" role="tab">${cat.catalogName}</a>

                                </c:forEach>
                            </div>
                        </div>
                    </div>
                    <div class="tab__container mt--60">
                        <!-- Start Single Tab Content -->
                        <div class="row single__tab tab-pane fade show active" id="nav-all" role="tabpanel">
                            <div class="product__indicator--4 arrows_style owl-carousel owl-theme">
                                <c:forEach items="${listProduct}" var="pro">
                                    <c:if test="${listProduct.indexOf(pro)==0 || (listProduct.indexOf(pro)%2) ==0}">
                                        <div class="single__product">
                                            <!-- Start Single Product -->
                                            <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                                                <div class="product product__style--3">
                                                    <div class="product__thumb">
                                                        <a class="first__img" href="initSingleProduct.htm?productId=${pro.productId}"><img src="/e-commerce-website/jsp/images/${pro.images}" height="300px" alt="product image"></a>
                                                            <c:if test="${pro.quantity==0}">
                                                            <div class="hot__box">
                                                                <span class="hot-label">SOLD OUT</span>
                                                            </div>
                                                        </c:if>
                                                    </div>
                                                    <div class="product__content content--center content--center">
                                                        <h4><a href="initSingleProduct.htm?productId=${pro.productId}">${pro.productName}</a></h4>
                                                        <ul class="prize d-flex">
                                                            <li>$<fmt:formatNumber  value="${pro.price*(100-pro.discount)/100}"></fmt:formatNumber></li>
                                                                <c:if test="${pro.discount!=0}">
                                                                <li class="old_prize">$<fmt:formatNumber  value="${pro.price}"></fmt:formatNumber></li>
                                                                </c:if>
                                                        </ul>
                                                        <div class="action">
                                                            <div class="actions_inner">
                                                                <ul class="add_to_links">
                                                                    <li><a class="wishlist" title="Add Cart" href="initAddCart1.htm?productId=${pro.productId}"><i class="bi bi-shopping-cart-full"></i></a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="product__hover--content">
                                                            <ul class="rating d-flex">
                                                                <li class="on"><i class="fa fa-star-o"></i></li>
                                                                <li class="on"><i class="fa fa-star-o"></i></li>
                                                                <li class="on"><i class="fa fa-star-o"></i></li>
                                                                <li><i class="fa fa-star-o"></i></li>
                                                                <li><i class="fa fa-star-o"></i></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </c:if>
                                        <c:if test="${listProduct.indexOf(pro)!=0 && (listProduct.indexOf(pro)%2) !=0}">
                                            <!-- Start Single Product -->
                                            <!-- Start Single Product -->
                                            <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                                                <div class="product product__style--3">
                                                    <div class="product__thumb">
                                                        <a class="first__img" href="initSingleProduct.htm?productId=${pro.productId}"><img src="/e-commerce-website/jsp/images/${pro.images}" height="300px" alt="product image"></a>
                                                            <c:if test="${pro.quantity==0}">
                                                            <div class="hot__box">
                                                                <span class="hot-label">SOLD OUT</span>
                                                            </div>
                                                        </c:if>
                                                    </div>
                                                    <div class="product__content content--center content--center">
                                                        <h4><a href="initSingleProduct.htm?productId=${pro.productId}">${pro.productName}</a></h4>
                                                        <ul class="prize d-flex">
                                                            <li>$<fmt:formatNumber  value="${pro.price*(100-pro.discount)/100}"></fmt:formatNumber></li>
                                                                <c:if test="${pro.discount!=0}">
                                                                <li class="old_prize">$<fmt:formatNumber  value="${pro.price}"></fmt:formatNumber></li>
                                                                </c:if>
                                                        </ul>
                                                        <div class="action">
                                                            <div class="actions_inner">
                                                                <ul class="add_to_links">
                                                                    <li><a class="wishlist" title="Add Cart" href="initAddCart1.htm?productId=${pro.productId}"><i class="bi bi-shopping-cart-full"></i></a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="product__hover--content">
                                                            <ul class="rating d-flex">
                                                                <li class="on"><i class="fa fa-star-o"></i></li>
                                                                <li class="on"><i class="fa fa-star-o"></i></li>
                                                                <li class="on"><i class="fa fa-star-o"></i></li>
                                                                <li><i class="fa fa-star-o"></i></li>
                                                                <li><i class="fa fa-star-o"></i></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Start Single Product -->

                                        </div>
                                    </c:if>
                                    <c:if test="${(listProduct.size()-listProduct.indexOf(pro))==1 && (listProduct.indexOf(pro)%2)==0}">
                                    </div>
                                </c:if>
                            </c:forEach>
                        </div>
                    </div>
                    <!-- End Single Tab Content -->
                    <!-- Start Single Tab Content -->
                    <c:forEach items="${listCatalog}" var="cat">
                        <div class="row single__tab tab-pane fade" id="${cat.catalogId}" role="tabpanel">
                            <div class="product__indicator--4 arrows_style owl-carousel owl-theme">
                                <c:forEach items="${listArrayProduct.get(listCatalog.indexOf(cat))}" var="pro">
                                    <c:if test="${listArrayProduct.get(listCatalog.indexOf(cat)).indexOf(pro)==0 || (listArrayProduct.get(listCatalog.indexOf(cat)).indexOf(pro)%2) ==0}">
                                        <div class="single__product">
                                            <!-- Start Single Product -->
                                            <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                                                <div class="product product__style--3">
                                                    <div class="product__thumb">
                                                        <a class="first__img" href="initSingleProduct.htm?productId=${pro.productId}"><img src="/e-commerce-website/jsp/images/${pro.images}" height="300px" alt="product image"></a>
                                                            <c:if test="${pro.quantity==0}">
                                                            <div class="hot__box">
                                                                <span class="hot-label">SOLD OUT</span>
                                                            </div>
                                                        </c:if>
                                                    </div>
                                                    <div class="product__content content--center content--center">
                                                        <h4><a href="initSingleProduct.htm?productId=${pro.productId}">${pro.productName}</a></h4>
                                                        <ul class="prize d-flex">
                                                            <li>$<fmt:formatNumber  value="${pro.price*(100-pro.discount)/100}"></fmt:formatNumber></li>
                                                                <c:if test="${pro.discount!=0}">
                                                                <li class="old_prize">$<fmt:formatNumber  value="${pro.price}"></fmt:formatNumber></li>
                                                                </c:if>
                                                        </ul>
                                                        <div class="action">
                                                            <div class="actions_inner">
                                                                <ul class="add_to_links">
                                                                    <li><a class="wishlist" title="Add Cart" href="initAddCart1.htm?productId=${pro.productId}"><i class="bi bi-shopping-cart-full"></i></a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="product__hover--content">
                                                            <ul class="rating d-flex">
                                                                <li class="on"><i class="fa fa-star-o"></i></li>
                                                                <li class="on"><i class="fa fa-star-o"></i></li>
                                                                <li class="on"><i class="fa fa-star-o"></i></li>
                                                                <li><i class="fa fa-star-o"></i></li>
                                                                <li><i class="fa fa-star-o"></i></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </c:if>
                                        <c:if test="${listArrayProduct.get(listCatalog.indexOf(cat)).indexOf(pro)!=0 && (listArrayProduct.get(listCatalog.indexOf(cat)).indexOf(pro)%2) !=0}">
                                            <!-- Start Single Product -->
                                            <!-- Start Single Product -->
                                            <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                                                <div class="product product__style--3">
                                                    <div class="product__thumb">
                                                        <a class="first__img" href="initSingleProduct.htm?productId=${pro.productId}"><img src="/e-commerce-website/jsp/images/${pro.images}" height="300px" alt="product image"></a>
                                                            <c:if test="${pro.quantity==0}">
                                                            <div class="hot__box">
                                                                <span class="hot-label">SOLD OUT</span>
                                                            </div>
                                                        </c:if>
                                                    </div>
                                                    <div class="product__content content--center content--center">
                                                        <h4><a href="initSingleProduct.htm?productId=${pro.productId}">${pro.productName}</a></h4>
                                                        <ul class="prize d-flex">
                                                            <li>$<fmt:formatNumber  value="${pro.price*(100-pro.discount)/100}"></fmt:formatNumber></li>
                                                                <c:if test="${pro.discount!=0}">
                                                                <li class="old_prize">$<fmt:formatNumber  value="${pro.price}"></fmt:formatNumber></li>
                                                                </c:if>
                                                        </ul>
                                                        <div class="action">
                                                            <div class="actions_inner">
                                                                <ul class="add_to_links">
                                                                    <li><a class="wishlist" title="Add Cart" href="initAddCart1.htm?productId=${pro.productId}"><i class="bi bi-shopping-cart-full"></i></a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="product__hover--content">
                                                            <ul class="rating d-flex">
                                                                <li class="on"><i class="fa fa-star-o"></i></li>
                                                                <li class="on"><i class="fa fa-star-o"></i></li>
                                                                <li class="on"><i class="fa fa-star-o"></i></li>
                                                                <li><i class="fa fa-star-o"></i></li>
                                                                <li><i class="fa fa-star-o"></i></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Start Single Product -->

                                        </div>
                                    </c:if>
                                    <c:if test="${(listArrayProduct.get(listCatalog.indexOf(cat)).size()-listArrayProduct.get(listCatalog.indexOf(cat)).indexOf(pro))==1 && (listArrayProduct.get(listCatalog.indexOf(cat)).indexOf(pro)%2)==0}">
                                    </div>
                                </c:if>
                            </c:forEach>
                        </div>
                    </div>
                </c:forEach>
                <!-- End Single Tab Content -->
        </div>
    </div>
</section>
<!-- Start BEst Seller Area -->
<!-- Start Recent Post Area -->
<!-- End Recent Post Area -->
<!-- Best Sale Area -->
<!-- Best Sale Area Area -->
<!-- Footer Area -->
<jsp:include page="Footer.jsp"></jsp:include>
    <!-- //Footer Area -->
    <!-- QUICKVIEW PRODUCT -->
    <!-- END QUICKVIEW PRODUCT -->
</div>
<!-- //Main wrapper -->

<!-- JS Files -->
<jsp:include page="Script.jsp"></jsp:include>
</body>

<!-- Mirrored from demo.devitems.com/boighor-v2/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 04 Apr 2019 10:36:31 GMT -->
</html>