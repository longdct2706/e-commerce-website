CREATE EXTENSION IF NOT EXISTS pgcrypto;
create table "Book"(
	bookid serial primary key,
	title varchar(100),
	author varchar(40),
	publisher varchar(100),
	byear int,
	genreid int,
	amount int,
	available int,
	price int,
	image text,
	description text
);

create table "User"(
	userid serial primary key,
	uname varchar(40),
	nickname varchar(40),
	password text,
	email varchar(40),
	address text,
	dob date,
	phonenumber varchar(20)
);

create table "Order"(
	orderid serial primary key,
	order_date date,
	total_price int,
	deliver_address text,
	note text
);

create table "Cart"(
	bookid int,
	orderid int,
	primary key (bookid, orderid),
	foreign key(bookid) references "Book"(bookid)
	on update cascade on delete cascade,
	foreign key(orderid) references "Order"(orderid)
	on update cascade on delete cascade,
	qty int,
	total_price int
);

create table "Order_log"(
	userid int,
	orderid int,
	primary key (userid, orderid),
	foreign key(userid) references "User"(userid)
	on update cascade on delete cascade,
	foreign key(orderid) references "Order"(orderid)
	on update cascade on delete cascade
)