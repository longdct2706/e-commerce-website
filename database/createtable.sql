create table "Users"(
	UserId int primary key generated always as identity,
	UserName varchar(100),
	Pass varchar(100),
	Email varchar(100),
	Address varchar(100),
	Phone varchar(20),
	Created date default current_date,
	IsAdmin bit,
	Status bit
);

create table "Product"(
	ProductId int primary key generated always as identity,
	ProductName varchar(200),
	ProductContent text,
	Images text,
	Price float,
	Quantity int,
	Catalog varchar(100),
	Created date default current_date,
	Status bit,
	ImageHover varchar(100),
	Discount int
);

create table "Orders"(
	OrderId int primary key generated always as identity,
	UserName varchar(100),
	CustomerName varchar(100),
	TotalAmount float,
	Address varchar(200),
	Phone varchar(15),
	CreatedDate date default current_date,
	PaymentMethod varchar(100),
	Status bit
);

create table "OrderDetail"(
	OrderDetailId int primary key generated always as identity,
	ProductId int,
	--ProductName varchar(100),
	OrderId int,
	UserId int,
	--UserName varchar(100),
	Quantity int,
	TotalPrice float,
	CreatedDate date default current_date,
	Status bit,
	constraint FK_OrderDetail_2 foreign key (OrderId)
		references "Orders" (OrderId) on delete cascade,
	constraint FK_OrderDetail_3 foreign key (ProductId)
		references "Product" (ProductId) on delete cascade,
	constraint FK_OrderDetail_4 foreign key (UserId)
		references "Users" (UserId) on delete cascade
);


