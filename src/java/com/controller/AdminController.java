/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controller;

import com.entities.Catalog;
import com.entities.OrderDetail;
import com.entities.Orders;
import com.entities.Product;
import com.entities.ProductInput;
import com.entities.ProductUpdate;
import com.entities.Users;
import com.model.CatalogModel;
import com.model.OrderModel;
import com.model.ProductModel;
import com.model.UserModel;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author H
 */
@Controller
@RequestMapping(value = "/AdminController")
public class AdminController {

    private ProductModel proModel;
    private CatalogModel catModel;
    private OrderModel ordModel;
    private UserModel userModel;

    public AdminController() {
        proModel = new ProductModel();
        catModel = new CatalogModel();
        ordModel = new OrderModel();
        userModel = new UserModel();
    }

    @RequestMapping(value = "/getIndex")
    public ModelAndView getIndex(HttpSession session) {
        String account = (String) session.getAttribute("account");
        if (account != null) {
            ModelAndView model = new ModelAndView("Admin/Index");
            List<Orders> listOrderProcess = ordModel.getOrderProcess();
            List<Product> listProductRunningOut = proModel.getProductRunningOut();
            List<Users> listCustomer = userModel.getListCustomer();
            model.addObject("totalCustomer", listCustomer.size());
            model.addObject("totalProductRunningOut", listProductRunningOut.size());
            model.addObject("totalOrderProcess", listOrderProcess.size());
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date date = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            int year = cal.get(Calendar.YEAR);
            int month = cal.get(Calendar.MONTH) + 1;
            for (int i = 1; i <= month; i++) {
                List<OrderDetail> listIncome = ordModel.getListIncome(i);
                float totalIncome = 0;
                for (OrderDetail income : listIncome) {
                    totalIncome += income.getTotalPrice();
                }
                model.addObject("totalIncome" + i, totalIncome);
            }

            model.addObject("month", month);
            model.addObject("year", year);
            return model;
        } else {
            ModelAndView model = new ModelAndView("Admin/Login");
            return model;
        }
    }

    @RequestMapping(value = "/getListOrderProcess")
    public ModelAndView getListOrderProcess(HttpSession session) {
        String account = (String) session.getAttribute("account");
        if (account != null) {
            ModelAndView model = new ModelAndView("Admin/OrderProcess");
            List<Orders> listOrderProcess = ordModel.getOrderProcess();
            model.addObject("listOrderProcess", listOrderProcess);
            return model;
        } else {
            ModelAndView model = new ModelAndView("Admin/Login");
            return model;
        }
    }

    @RequestMapping(value = "/getAllOrder")
    public ModelAndView getAllOrder(HttpSession session) {
        String account = (String) session.getAttribute("account");
        if (account != null) {
            ModelAndView model = new ModelAndView("Admin/Order");
            List<Orders> listOrder = ordModel.getAllOrder();
            model.addObject("listOrder", listOrder);
            return model;
        } else {
            ModelAndView model = new ModelAndView("Admin/Login");
            return model;
        }
    }

    @RequestMapping(value = "/getAllProduct")
    public ModelAndView getAllProduct(HttpSession session) {
        String account = (String) session.getAttribute("account");
        if (account != null) {
            ModelAndView model = new ModelAndView("Admin/Product");
            List<Product> listProduct = proModel.getAllProduct();
            List<Catalog> listCatalog = catModel.getAllCatalog();
            model.addObject("listCatalog", listCatalog);
            model.addObject("listProduct", listProduct);
            return model;
        } else {
            ModelAndView model = new ModelAndView("Admin/Login");
            return model;
        }
    }

    @RequestMapping(value = "/initInsertProduct")
    public ModelAndView initInsertProduct(String message, ModelMap mm, HttpSession session) {
        String account = (String) session.getAttribute("account");
        if (account != null) {
            ModelAndView model = new ModelAndView("Admin/ProductInsert");
            ProductInput proNew = new ProductInput();
            List<Catalog> listCatalog = catModel.getAllCatalog();
            mm.put("message", message);
            model.addObject("listCatalog", listCatalog);
            model.addObject("proNew", proNew);
            return model;
        } else {
            ModelAndView model = new ModelAndView("Admin/Login");
            return model;
        }
    }

    @RequestMapping(value = "/insertProduct")
    public String insertProduct(ProductInput proNew, ModelMap mm, HttpServletRequest request, HttpSession session) {

        MultipartFile file = proNew.getImages();
        File serverFile = null;
        if (!file.isEmpty()) {
            try {
                byte[] bytes = file.getBytes();
                String path = request.getRealPath("jsp/images/");
                path = path.substring(0, path.indexOf("\\build"));
                path = path + "\\web\\jsp\\images";
                serverFile = new File(path + File.separator + file.getOriginalFilename());
                System.out.println("AAAA:" + serverFile.getPath());
                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
                stream.write(bytes);
                stream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        MultipartFile file1 = proNew.getImageHover();
        File serverFile1 = null;
        if (!file1.isEmpty()) {
            try {
                byte[] bytes1 = file1.getBytes();
                String path1 = request.getRealPath("jsp/images/");
                path1 = path1.substring(0, path1.indexOf("\\build"));
                path1 = path1 + "\\web\\jsp\\images";
                serverFile1 = new File(path1 + File.separator + file1.getOriginalFilename());
                System.out.println("AAAA:" + serverFile1.getPath());
                BufferedOutputStream stream1 = new BufferedOutputStream(new FileOutputStream(serverFile1));
                stream1.write(bytes1);
                stream1.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Product pro = new Product();
        pro.setProductName(proNew.getProductName());
        pro.setProductContent(proNew.getProductContent());
        pro.setPrice(proNew.getPrice());
        pro.setCatalogId(proNew.getCatalogId());
        pro.setImages(file.getOriginalFilename());
        pro.setImageHover(file1.getOriginalFilename());
        pro.setDiscount(proNew.getDiscount());
        pro.setQuantity(proNew.getQuantity());

        boolean check = proModel.insertProduct(pro);
        if (check) {
            mm.put("message", "You have inserted successfully ! ");
        } else {
            mm.put("message", "You have inserted failed ! ");
        }
        return "redirect:initInsertProduct.htm";
    }

    @RequestMapping(value = "/initUpdateProduct")
    public ModelAndView initUpdateProduct(int productId, String message, ModelMap mm, HttpServletRequest request, HttpSession session) {
        String account = (String) session.getAttribute("account");
        if (account != null) {
            ModelAndView model = new ModelAndView("Admin/ProductUpdate");
            Product pro = proModel.getProductByProductId(productId);
            ProductUpdate proUpdate = new ProductUpdate();
            proUpdate.setProductId(pro.getProductId());
            proUpdate.setProductName(pro.getProductName());
            proUpdate.setProductContent(pro.getProductContent());
            proUpdate.setPrice(pro.getPrice());
            proUpdate.setCatalogId(pro.getCatalogId());
            proUpdate.setImages(pro.getImages());
            proUpdate.setImageHover(pro.getImageHover());
            proUpdate.setQuantity(pro.getQuantity());
            proUpdate.setDiscount(pro.getDiscount());
            proUpdate.setStatus(pro.isStatus());
            mm.put("message", message);
            model.addObject("proUpdate", proUpdate);
            return model;
        } else {
            ModelAndView model = new ModelAndView("Admin/Login");
            return model;
        }
    }

    @RequestMapping(value = "/updateProduct")
    public String updateProduct(ProductUpdate proUpdate, ModelMap mm, HttpServletRequest request, HttpSession session) {
        String account = (String) session.getAttribute("account");
        if (account != null) {
            Product pro = new Product();
            pro.setProductName(proUpdate.getProductName());
            pro.setProductContent(proUpdate.getProductContent());
            pro.setPrice(proUpdate.getPrice());
            pro.setCatalogId(proUpdate.getCatalogId());
            pro.setImages(proUpdate.getImages());
            pro.setImageHover(proUpdate.getImageHover());
            pro.setDiscount(proUpdate.getDiscount());
            pro.setQuantity(proUpdate.getQuantity());
            pro.setStatus(proUpdate.isStatus());
            pro.setProductId(proUpdate.getProductId());
            MultipartFile file = proUpdate.getImagesUpdate();
            File serverFile = null;
            if (!file.isEmpty()) {
                try {
                    byte[] bytes = file.getBytes();
                    String path = request.getRealPath("jsp/images/");
                    path = path.substring(0, path.indexOf("\\build"));
                    path = path + "\\web\\jsp\\images";
                    serverFile = new File(path + File.separator + file.getOriginalFilename());
                    System.out.println("AAAA:" + serverFile.getPath());
                    BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
                    stream.write(bytes);
                    stream.close();
                    pro.setImages(file.getOriginalFilename());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            MultipartFile file1 = proUpdate.getImageHoverUpdate();
            File serverFile1 = null;
            if (!file1.isEmpty()) {
                try {
                    byte[] bytes1 = file1.getBytes();
                    String path1 = request.getRealPath("jsp/images/");
                    path1 = path1.substring(0, path1.indexOf("\\build"));
                    path1 = path1 + "\\web\\jsp\\images";
                    serverFile1 = new File(path1 + File.separator + file1.getOriginalFilename());
                    System.out.println("AAAA:" + serverFile1.getPath());
                    BufferedOutputStream stream1 = new BufferedOutputStream(new FileOutputStream(serverFile1));
                    stream1.write(bytes1);
                    stream1.close();
                    pro.setImageHover(file1.getOriginalFilename());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            boolean check = proModel.updateProduct(pro);
            if (check) {
                mm.put("message", "You have updated successfully ! ");
            } else {
                mm.put("message", "You have updated failed ! ");
            }
            return "redirect:initUpdateProduct.htm?productId=" + proUpdate.getProductId();
        } else {
            return "redirect:getLogin.htm";
        }
    }

    @RequestMapping(value = "/initUpdateProduct1")
    public ModelAndView initUpdateProduct1(int productId, String message, ModelMap mm, HttpServletRequest request, HttpSession session) {
        String account = (String) session.getAttribute("account");
        if (account != null) {
            ModelAndView model = new ModelAndView("Admin/ProductUpdate1");
            Product pro = proModel.getProductByProductId(productId);
            ProductUpdate proUpdate = new ProductUpdate();
            proUpdate.setProductId(pro.getProductId());
            proUpdate.setProductName(pro.getProductName());
            proUpdate.setProductContent(pro.getProductContent());
            proUpdate.setPrice(pro.getPrice());
            proUpdate.setCatalogId(pro.getCatalogId());
            proUpdate.setImages(pro.getImages());
            proUpdate.setImageHover(pro.getImageHover());
            proUpdate.setQuantity(pro.getQuantity());
            proUpdate.setDiscount(pro.getDiscount());
            proUpdate.setStatus(pro.isStatus());
            mm.put("message", message);
            model.addObject("proUpdate", proUpdate);
            return model;
        } else {
            ModelAndView model = new ModelAndView("Admin/Login");
            return model;
        }
    }

    @RequestMapping(value = "/updateProduct1")
    public String updateProduct1(ProductUpdate proUpdate, ModelMap mm, HttpServletRequest request, HttpSession session) {
        String account = (String) session.getAttribute("account");
        if (account != null) {
            Product pro = new Product();
            pro.setProductName(proUpdate.getProductName());
            pro.setProductContent(proUpdate.getProductContent());
            pro.setPrice(proUpdate.getPrice());
            pro.setCatalogId(proUpdate.getCatalogId());
            pro.setImages(proUpdate.getImages());
            pro.setImageHover(proUpdate.getImageHover());
            pro.setDiscount(proUpdate.getDiscount());
            pro.setQuantity(proUpdate.getQuantity());
            pro.setStatus(proUpdate.isStatus());
            pro.setProductId(proUpdate.getProductId());
            MultipartFile file = proUpdate.getImagesUpdate();
            File serverFile = null;
            if (!file.isEmpty()) {
                try {
                    byte[] bytes = file.getBytes();
                    String path = request.getRealPath("jsp/images/");
                    path = path.substring(0, path.indexOf("\\build"));
                    path = path + "\\web\\jsp\\images";
                    serverFile = new File(path + File.separator + file.getOriginalFilename());
                    System.out.println("AAAA:" + serverFile.getPath());
                    BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
                    stream.write(bytes);
                    stream.close();
                    pro.setImages(file.getOriginalFilename());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            MultipartFile file1 = proUpdate.getImageHoverUpdate();
            File serverFile1 = null;
            if (!file1.isEmpty()) {
                try {
                    byte[] bytes1 = file1.getBytes();
                    String path1 = request.getRealPath("jsp/images/");
                    path1 = path1.substring(0, path1.indexOf("\\build"));
                    path1 = path1 + "\\web\\jsp\\images";
                    serverFile1 = new File(path1 + File.separator + file1.getOriginalFilename());
                    System.out.println("AAAA:" + serverFile1.getPath());
                    BufferedOutputStream stream1 = new BufferedOutputStream(new FileOutputStream(serverFile1));
                    stream1.write(bytes1);
                    stream1.close();
                    pro.setImageHover(file1.getOriginalFilename());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            boolean check = proModel.updateProduct(pro);
            if (check) {
                mm.put("message", "You have updated successfully ! ");
            } else {
                mm.put("message", "You have updated failed ! ");
            }
            return "redirect:initUpdateProduct1.htm?productId=" + proUpdate.getProductId();
        } else {
            return "redirect:getLogin.htm";
        }
    }

    @RequestMapping(value = "/deleteProduct")
    public String deleteProduct(int productId, HttpSession session) {
        String account = (String) session.getAttribute("account");
        if (account != null) {
            boolean check = proModel.deleteProduct(productId);
            if (check) {
                return "redirect:getAllProduct.htm";
            } else {
                return "Admin/error";
            }
        } else {
            return "redirect:getLogin.htm";
        }
    }

    @RequestMapping(value = "/getAllCatalog")
    public ModelAndView getAllCatalog(HttpSession session) {
        String account = (String) session.getAttribute("account");
        if (account != null) {
            ModelAndView model = new ModelAndView("Admin/Catalog");
            List<Catalog> listCatalog = catModel.getAllCatalog();
            model.addObject("listCatalog", listCatalog);
            return model;
        } else {
            ModelAndView model = new ModelAndView("Admin/Login");
            return model;
        }
    }

    @RequestMapping(value = "/initInsertCatalog")
    public ModelAndView initInsertCatalog(ModelMap mm, String message, HttpSession session) {
        String account = (String) session.getAttribute("account");
        if (account != null) {
            ModelAndView model = new ModelAndView("Admin/CatalogInsert");
            Catalog catNew = new Catalog();
            model.addObject("catNew", catNew);
            mm.put("message", message);
            return model;
        } else {
            ModelAndView model = new ModelAndView("Admin/Login");
            return model;
        }
    }

    @RequestMapping(value = "/insertCatalog")
    public String insertCatalog(Catalog catNew, ModelMap mm, HttpSession session) {
        String account = (String) session.getAttribute("account");
        if (account != null) {
            boolean check = catModel.insertCatalog(catNew);
            if (check) {
                mm.put("message", "You have inserted successfully !");
            } else {
                mm.put("message", "You have inserted failed !");
            }
            return "redirect:initInsertCatalog.htm";
        } else {
            return "redirect:getLogin.htm";
        }
    }

    @RequestMapping(value = "/initUpdateCatalog")
    public ModelAndView initUpdateCatalog(int catalogId, ModelMap mm, String message, HttpSession session) {
        String account = (String) session.getAttribute("account");
        if (account != null) {
            ModelAndView model = new ModelAndView("Admin/CatalogUpdate");
            Catalog catUpdate = catModel.getCatalogByCatalogId(catalogId);
            model.addObject("catUpdate", catUpdate);
            mm.put("message", message);
            return model;
        } else {
            ModelAndView model = new ModelAndView("Admin/Login");
            return model;
        }
    }

    @RequestMapping(value = "/updateCatalog")
    public String updateCatalog(Catalog catUpdate, ModelMap mm, HttpSession session) {
        String account = (String) session.getAttribute("account");
        if (account != null) {
            boolean check = catModel.updateCatalog(catUpdate);
            if (check) {
                mm.put("message", "You have updated successfully !");
            } else {
                mm.put("message", "You have updated failed !");
            }
            return "redirect:initUpdateCatalog.htm?catalogId=" + catUpdate.getCatalogId();
        } else {
            return "redirect:getLogin.htm";
        }
    }

    @RequestMapping(value = "/deleteCatalog")
    public String deleteCatalog(int catalogId, HttpSession session) {
        String account = (String) session.getAttribute("account");
        if (account != null) {
            boolean check = catModel.deleteCatalog(catalogId);
            if (check) {
                return "redirect:getAllCatalog.htm";
            } else {
                return "Admin/error";
            }
        } else {
            return "redirect:getLogin.htm";
        }
    }

    @RequestMapping(value = "/getOrderDetailByOrderId")
    public ModelAndView getOrderDetailByOrderId(int orderId, HttpSession session) {
        String account = (String) session.getAttribute("account");
        if (account != null) {
            ModelAndView model = new ModelAndView("Admin/OrderDetail");
            List<OrderDetail> listOrderDetail = ordModel.getOrderDetailByOrderId(orderId);
            model.addObject("listOrderDetail", listOrderDetail);
            return model;
        } else {
            ModelAndView model = new ModelAndView("Admin/Login");
            return model;
        }
    }

    @RequestMapping(value = "/getOrderDetailByOrderId1")
    public ModelAndView getOrderDetailByOrderId1(int orderId, HttpSession session) {
        String account = (String) session.getAttribute("account");
        if (account != null) {
            ModelAndView model = new ModelAndView("Admin/OrderDetail1");
            List<OrderDetail> listOrderDetail = ordModel.getOrderDetailByOrderId(orderId);
            model.addObject("listOrderDetail", listOrderDetail);
            return model;
        } else {
            ModelAndView model = new ModelAndView("Admin/Login");
            return model;
        }
    }

    @RequestMapping(value = "/initUpdateOrder")
    public ModelAndView initUpdateOrder(int orderId, String message, HttpSession session) {
        String account = (String) session.getAttribute("account");
        if (account != null) {
            ModelAndView model = new ModelAndView("Admin/OrderUpdate");
            Orders orderUpdate = ordModel.getOrderByOrderId(orderId);
            model.addObject("orderUpdate", orderUpdate);
            model.addObject("message", message);
            return model;
        } else {
            ModelAndView model = new ModelAndView("Admin/Login");
            return model;
        }
    }

    @RequestMapping(value = "/updateOrder")
    public String updateOrder(Orders orderUpdate, ModelMap mm, HttpSession session) {
        String account = (String) session.getAttribute("account");
        if (account != null) {
            boolean check = ordModel.updateOrder(orderUpdate);
            if (check) {
                mm.put("message", "You have updated successfully !");
            } else {
                mm.put("message", "You have updated failed !");
            }
            return "redirect:initUpdateOrder.htm?orderId=" + orderUpdate.getOrderId();
        } else {
            return "redirect:getLogin.htm";
        }
    }

    @RequestMapping(value = "/initUpdateOrder1")
    public ModelAndView initUpdateOrder1(int orderId, String message, HttpSession session) {
        String account = (String) session.getAttribute("account");
        if (account != null) {
            ModelAndView model = new ModelAndView("Admin/OrderUpdate1");
            Orders orderUpdate = ordModel.getOrderByOrderId(orderId);
            model.addObject("orderUpdate", orderUpdate);
            model.addObject("message", message);
            return model;
        } else {
            ModelAndView model = new ModelAndView("Admin/Login");
            return model;
        }
    }

    @RequestMapping(value = "/updateOrder1")
    public String updateOrder1(Orders orderUpdate, ModelMap mm, HttpSession session) {
        String account = (String) session.getAttribute("account");
        if (account != null) {
            boolean check = ordModel.updateOrder(orderUpdate);
            if (check) {
                mm.put("message", "You have updated successfully !");
            } else {
                mm.put("message", "You have updated failed !");
            }
            return "redirect:initUpdateOrder1.htm?orderId=" + orderUpdate.getOrderId();
        } else {
            return "redirect:getLogin.htm";
        }
    }

    @RequestMapping(value = "/deleteOrder")
    public String deleteOrder(int orderId, HttpSession session) {
        String account = (String) session.getAttribute("account");
        if (account != null) {
            List<OrderDetail> listOrderDetail = ordModel.getOrderDetailByOrderId(orderId);
            if (ordModel.recoverQuantity(listOrderDetail)) {
                if (ordModel.deleteOrder(orderId)) {
                    return "redirect:getAllOrder.htm";
                } else {
                    return "Admin/error";
                }
            } else {
                return "Admin/error";
            }
        } else {
            return "redirect:getLogin.htm";
        }
    }

    @RequestMapping(value = "/deleteOrder1")
    public String deleteOrder1(int orderId, HttpSession session) {
        String account = (String) session.getAttribute("account");
        if (account != null) {
            List<OrderDetail> listOrderDetail = ordModel.getOrderDetailByOrderId(orderId);
            if (ordModel.recoverQuantity(listOrderDetail)) {
                if (ordModel.deleteOrder(orderId)) {
                    return "redirect:getListOrderProcess.htm";
                } else {
                    return "Admin/error";
                }
            } else {
                return "Admin/error";
            }
        } else {
            return "redirect:getLogin.htm";
        }
    }

    @RequestMapping(value = "/getAllUsers")
    public ModelAndView getAllUsers(HttpSession session) {
        String account = (String) session.getAttribute("account");
        if (account != null) {
            ModelAndView model = new ModelAndView("Admin/Users");
            List<Users> listUsers = userModel.getAllUsers();
            model.addObject("listUsers", listUsers);
            return model;
        } else {
            ModelAndView model = new ModelAndView("Admin/Login");
            return model;
        }
    }

    @RequestMapping(value = "/getProductRunningOut")
    public ModelAndView getProductRunningOut(HttpSession session) {
        String account = (String) session.getAttribute("account");
        if (account != null) {
            ModelAndView model = new ModelAndView("Admin/ProductRunningOut");
            List<Product> listProduct = proModel.getProductRunningOut();
            List<Catalog> listCatalog = catModel.getAllCatalog();
            model.addObject("listCatalog", listCatalog);
            model.addObject("listProduct", listProduct);
            return model;
        } else {
            ModelAndView model = new ModelAndView("Admin/Login");
            return model;
        }
    }

    @RequestMapping(value = "/getListCustomer")
    public ModelAndView getListCustomer(HttpSession session) {
        String account = (String) session.getAttribute("account");
        if (account != null) {
            ModelAndView model = new ModelAndView("Admin/Users");
            List<Users> listUsers = userModel.getListCustomer();
            model.addObject("listUsers", listUsers);
            return model;
        } else {
            ModelAndView model = new ModelAndView("Admin/Login");
            return model;
        }
    }

    @RequestMapping(value = "/getListIncome")
    public ModelAndView getListIncome(int month, HttpSession session) {
        String account = (String) session.getAttribute("account");
        if (account != null) {
            ModelAndView model = new ModelAndView("Admin/Income");
            List<OrderDetail> listIncome = ordModel.getListIncome(month);
            model.addObject("listIncome", listIncome);
            return model;
        } else {
            ModelAndView model = new ModelAndView("Admin/Login");
            return model;
        }
    }

    @RequestMapping(value = "/getLogin")
    public ModelAndView getLogin() {
        ModelAndView model = new ModelAndView("Admin/Login");
        return model;
    }

    @RequestMapping(value = "/checkLoginAdmin")
    public String checkLoginAdmin(String userName, String pass, HttpSession session, ModelMap mm) {
        boolean check = userModel.checkLoginAdmin(userName, pass);
        Users user = userModel.getUserByUserName(userName);
        if (check) {
            session.setAttribute("account", userName);
            session.setAttribute("created", user.getCreated());
            return "redirect:getIndex.htm";
        } else {
            mm.put("message", "Wrong Login Information !");
            return "Admin/Login";
        }
    }

    @RequestMapping(value = "/logout")
    public String logout(HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.invalidate();
        return "redirect:getLogin.htm";
    }
}
