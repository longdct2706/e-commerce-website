/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controller;

import com.entities.Cart;
import com.entities.Catalog;
import com.entities.OrderDetail;
import com.entities.Orders;
import com.entities.Product;
import com.entities.Users;
import com.model.CatalogModel;
import com.model.OrderModel;
import com.model.PaymentModel;
import com.model.ProductModel;
import com.model.UserModel;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author H
 */
@Controller
@RequestMapping(value = "/UserController")
public class UserController {

    private ProductModel proModel;
    private CatalogModel catModel;
    private UserModel userModel;
    private PaymentModel payModel;
    private OrderModel ordModel;

    public UserController() {
        proModel = new ProductModel();
        catModel = new CatalogModel();
        userModel = new UserModel();
        payModel = new PaymentModel();
        ordModel = new OrderModel();
    }

    @RequestMapping(value = "/getAllProduct")
    public ModelAndView getAllProduct(HttpSession session) {
        ModelAndView model = new ModelAndView("Frontend/Index");
        List<Product> listProduct = proModel.getAllProduct();
        List<Catalog> listCatalog = catModel.getAllCatalog();
        List<Product> listBestProduct = proModel.getBestProduct();
        ArrayList<List<Product>> listArrayProduct = new ArrayList<>();
        for (int i = 0; i < listCatalog.size(); i++) {
            List<Product> listPro = proModel.getProductByCatalogId(listCatalog.get(i).getCatalogId());
            listArrayProduct.add(listPro);
        }
        model.addObject("listProduct", listProduct);
        model.addObject("listBestProduct", listBestProduct);
        session.setAttribute("listCatalog", listCatalog);
        model.addObject("listArrayProduct", listArrayProduct);
        return model;
    }

    @RequestMapping(value = "/getAllProductShopList")
    public ModelAndView getAllProductShopList() {
        ModelAndView model = new ModelAndView("Frontend/ShopList");
        List<Product> listProduct = proModel.getAllProduct();
        List<Catalog> listCatalog = catModel.getAllCatalog();
        model.addObject("listCatalog", listCatalog);
        model.addObject("listProduct", listProduct);
        return model;
    }

    @RequestMapping(value = "/getProductByCatalogId")
    public ModelAndView getProductByCatalogId(int catalogId) {
        ModelAndView model = new ModelAndView("Frontend/ShopList");
        List<Product> listProduct = proModel.getProductByCatalogId(catalogId);
        List<Catalog> listCatalog = catModel.getAllCatalog();
        model.addObject("listCatalog", listCatalog);
        model.addObject("listProduct", listProduct);
        model.addObject("catalogId", catalogId);
        return model;
    }

    @RequestMapping(value = "/initSingleProduct")
    public ModelAndView initSingleProduct(int productId, String message, ModelMap mm) {
        ModelAndView model = new ModelAndView("Frontend/SingleProduct");
        Product pro = proModel.getProductByProductId(productId);
        mm.put("message", message);
        model.addObject("pro", pro);
        return model;
    }

    @RequestMapping(value = "/initShopingCart")
    public ModelAndView initShopingCart() {
        ModelAndView model = new ModelAndView("Frontend/ShopingCart");
        List<Catalog> listCatalog = catModel.getAllCatalog();
        model.addObject("listCatalog", listCatalog);
        return model;
    }

    @RequestMapping(value = "/initAddCart")
    public String initAddCart(int quantity, int productId, HttpSession session, ModelMap mm) {
        List<Cart> listCart = (List<Cart>) session.getAttribute("listCart");
        Product pro = proModel.getProductByProductId(productId);
        if (listCart == null) {
            if (pro.getQuantity() == 0) {
                mm.put("message", "This Product has sold out !");
                return "redirect:initSingleProduct.htm?productId=" + productId;
            } else if (quantity <= pro.getQuantity()) {
                listCart = new ArrayList<>();
                Cart cart = new Cart(pro, quantity);
                listCart.add(cart);
            } else {
                mm.put("message", "This Product only have " + pro.getQuantity() + " left !");
                return "redirect:initSingleProduct.htm?productId=" + productId;
            }
        } else {
            boolean check = false;
            for (Cart cart : listCart) {
                if (cart.getProduct().getProductId() == productId) {
                    if (cart.getQuantity() + quantity <= cart.getProduct().getQuantity()) {
                        cart.setQuantity(cart.getQuantity() + quantity);
                        check = true;
                        break;
                    } else {
                        mm.put("message", "This Product only have " + cart.getProduct().getQuantity() + " left ,Your cart already have " + cart.getQuantity());
                        return "redirect:initSingleProduct.htm?productId=" + productId;
                    }
                }
            }
            if (!check) {
                if (pro.getQuantity() == 0) {
                    mm.put("message", "This Product has sold out !");
                    return "redirect:initSingleProduct.htm?productId=" + productId;
                } else if (quantity <= pro.getQuantity()) {
                    Cart cart = new Cart(pro, quantity);
                    listCart.add(cart);
                } else {
                    mm.put("message", "This Product only have " + pro.getQuantity() + " left !");
                    return "redirect:initSingleProduct.htm?productId=" + productId;
                }
            }
        }
        session.setAttribute("listCart", listCart);
        session.setAttribute("totalAmount", calTotalAmount(listCart));
        return "redirect:addCart.htm";
    }

    @RequestMapping(value = "/initAddCart1")
    public String initAddCart1(int productId, HttpSession session, ModelMap mm) {
        List<Cart> listCart = (List<Cart>) session.getAttribute("listCart");
        Product pro = proModel.getProductByProductId(productId);
        if (listCart == null) {
            if (pro.getQuantity() == 0) {
                mm.put("message", "This Product has sold out !");
                return "redirect:initSingleProduct.htm?productId=" + productId;
            } else {
                listCart = new ArrayList<>();
                Cart cart = new Cart(pro, 1);
                listCart.add(cart);
            }
        } else {
            boolean check = false;
            for (Cart cart : listCart) {
                if (cart.getProduct().getProductId() == productId) {
                    if (cart.getQuantity() == cart.getProduct().getQuantity()) {
                        mm.put("message", "This Product only have " + cart.getProduct().getQuantity() + " left ,Your cart already have " + cart.getQuantity());
                        return "redirect:initSingleProduct.htm?productId=" + productId;
                    } else {
                        cart.setQuantity(cart.getQuantity() + 1);
                        check = true;
                        break;
                    }
                }
            }
            if (!check) {
                if (pro.getQuantity() == 0) {
                    mm.put("message", "This Product has sold out !");
                    return "redirect:initSingleProduct.htm?productId=" + productId;
                } else {
                    Cart cart = new Cart(pro, 1);
                    listCart.add(cart);
                }
            }
        }
        session.setAttribute("listCart", listCart);
        session.setAttribute("totalAmount", calTotalAmount(listCart));
        return "redirect:addCart.htm";
    }

    @RequestMapping(value = "/addCart")
    public ModelAndView addCart() {
        ModelAndView model = new ModelAndView("Frontend/ShopingCart");
        return model;
    }

    public float calTotalAmount(List<Cart> listCart) {
        float total = 0;
        for (int i = 0; i < listCart.size(); i++) {
            total += listCart.get(i).getQuantity() * listCart.get(i).getProduct().getPrice() * (100 - listCart.get(i).getProduct().getDiscount()) / 100;
        }
        return total;
    }

    @RequestMapping(value = "/updateQuantity")
    public ModelAndView updateQuantity(HttpSession session, HttpServletRequest request, ModelMap mm) {
        ModelAndView model = new ModelAndView("Frontend/ShopingCart");
        String arrQuantity[] = request.getParameterValues("quantity");
        List<Cart> listCart = (List<Cart>) session.getAttribute("listCart");
        List<String> listMessage = new ArrayList<>();
        String message = "";
        for (int i = 0; i < listCart.size(); i++) {
            Product pro = proModel.getProductByProductId(listCart.get(i).getProduct().getProductId()); // cap nhat lai so luong san pham tren sever
            listCart.get(i).setProduct(pro);
            if (Integer.parseInt(arrQuantity[i]) <= listCart.get(i).getProduct().getQuantity()) {
                listCart.get(i).setQuantity(Integer.parseInt(arrQuantity[i]));
                message = "";
                listMessage.add(message);
            } else if (listCart.get(i).getProduct().getQuantity() == 0) {
                message = "The Product has sold out !";
                listMessage.add(message);
            } else {
                message = "The Product has only " + listCart.get(i).getProduct().getQuantity() + " available";
                listMessage.add(message);
            }
        }
        mm.put("listMessage", listMessage);
        session.setAttribute("listCart", listCart);
        session.setAttribute("totalAmount", calTotalAmount(listCart));
        return model;
    }

    @RequestMapping(value = "/removeCart")
    public ModelAndView removeCart(int productId, HttpSession session) {
        ModelAndView model = new ModelAndView("Frontend/ShopingCart");
        List<Cart> listCart = (List<Cart>) session.getAttribute("listCart");
        for (int i = 0; i < listCart.size(); i++) {
            if (listCart.get(i).getProduct().getProductId() == productId) {
                listCart.remove(i);
                break;
            }
        }
        session.setAttribute("listCart", listCart);
        session.setAttribute("totalAmount", calTotalAmount(listCart));
        return model;
    }

    @RequestMapping(value = "/removeAllCart")
    public ModelAndView removeAllCart(HttpSession session) {
        ModelAndView model = new ModelAndView("Frontend/ShopingCart");
        List<Cart> listCart = (List<Cart>) session.getAttribute("listCart");
        int size = listCart.size();
        for (int i = 0; i < size; i++) {
            listCart.remove(0);

        }
        session.setAttribute("listCart", listCart);
        session.setAttribute("totalAmount", calTotalAmount(listCart));
        return model;
    }

    @RequestMapping(value = "/initLogin")
    public ModelAndView initLogin() {
        ModelAndView model = new ModelAndView("Frontend/Login");
        return model;
    }

    @RequestMapping(value = "/checkLogin")
    public String checkLogin(String userName, String pass, HttpSession session, ModelMap mm) {
        boolean check = userModel.checkLogin(userName, pass);
        if (check) {
            Users user = userModel.getUserByUserName(userName);
            session.setAttribute("fullName", user.getFullName());
            session.setAttribute("address", user.getAddress());
            session.setAttribute("phone", user.getPhone());
            session.setAttribute("email", user.getEmail());
            session.setAttribute("userName", userName);
            String[] parts = user.getFullName().split(" ");
            String accountName = parts[parts.length - 1];
            session.setAttribute("account", accountName);
            return "redirect:getAllProduct.htm";
        } else {
            mm.put("message", "Invalid Information !");
            return "Frontend/Login";
        }
    }

    @RequestMapping(value = "/logout")
    public String logout(HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.invalidate();
        return "redirect:getAllProduct.htm";
    }

    @RequestMapping(value = "/initRegister")
    public ModelAndView initRegister(String message, String message1, ModelMap mm) {
        ModelAndView model = new ModelAndView("Frontend/Register");
        Users users = new Users();
        mm.put("message", message);
        mm.put("message1", message1);
        model.addObject("users", users);
        return model;
    }

    @RequestMapping(value = "/registerUser")
    public String registerUser(Users users, ModelMap mm) {
        if (userModel.checkUsername(users.getUserName())) {
            mm.put("message", "Your Username already exists !");
        } else if (userModel.checkEmail(users.getEmail())) {
            mm.put("message", "Your Email already exists !");
        } else {
            boolean check = userModel.registerUser(users);
            if (check) {
                mm.put("message1", "You have successfully registered !");
            } else {
                mm.put("message", "Registration Failed !");
            }
        }
        return "redirect:initRegister.htm";
    }

    @RequestMapping(value = "/initCheckOut")
    public ModelAndView initCheckOut(ModelMap mm, String message) {
        ModelAndView model = new ModelAndView("Frontend/CheckOut");
        mm.put("message", message);
        return model;
    }

    @RequestMapping(value = "/initError")
    public ModelAndView initError(ModelMap mm, String message) {
        ModelAndView model = new ModelAndView("Frontend/Error");
        mm.put("message", message);
        return model;
    }

    @RequestMapping(value = "/initSuccess")
    public ModelAndView initSuccess() {
        ModelAndView model = new ModelAndView("Frontend/Success");
        return model;
    }

    @RequestMapping(value = "/shopingCart")
    public ModelAndView shopingCart(ModelMap mm, List<String> listMessage) {
        ModelAndView model = new ModelAndView("Frontend/ShopingCart");
        mm.put("listMessage", listMessage);
        return model;
    }

    @RequestMapping(value = "/executeOrder")
    public String executeOrder(String customerName, String paymentMethod, String address, String phone, String email, String cardNumber, String accountName, HttpSession session, ModelMap mm) {
        List<Cart> listCart = (List<Cart>) session.getAttribute("listCart");
        if (listCart != null && listCart.size() != 0) {
            float totalAmount = (float) session.getAttribute("totalAmount");
            String userName = (String) session.getAttribute("userName");
            int accountId = payModel.getAccountIdByCardNumber(cardNumber);
            if (totalAmount < 20) {
                totalAmount += 1;
            }
            if (paymentMethod.equals("Direct Bank Transfer")) { // Thanh toan chuyen khoan
                boolean check = payModel.checkPayment(accountName, cardNumber);
                if (check) {
                    List<String> listMessage = new ArrayList<>();
                    String message = "";
                    int count = 0; // check xem co false hay ko ma van hien ra thong bao
                    for (int i = 0; i < listCart.size(); i++) {
                        Product pro = proModel.getProductByProductId(listCart.get(i).getProduct().getProductId()); // cap nhat lai so luong san pham tren sever
                        listCart.get(i).setProduct(pro);
                        if (listCart.get(i).getQuantity() <= listCart.get(i).getProduct().getQuantity()) {
                            message = "";
                            listMessage.add(message);
                        } else if (listCart.get(i).getProduct().getQuantity() == 0) {
                            message = "The Product has sold out !";
                            listMessage.add(message);
                            count++;
                        } else {
                            message = "The Product has only " + listCart.get(i).getProduct().getQuantity() + " available";
                            listMessage.add(message);
                            count++;
                        }
                    }
                    if (count == 0) {
                        boolean check1 = payModel.executePayment(accountId, totalAmount);
                        if (check1) {
                            payModel.executeOrder(listCart);
                            boolean check2 = payModel.createOrder(userName, customerName, address, phone, email, paymentMethod, totalAmount, listCart);
                            if (check2) {
                                return "redirect:initSuccess.htm";
                            } else {
                                return "redirect:initError.htm";
                            }
                        } else {
                            ModelAndView model = new ModelAndView("Frontend/Error");
                            mm.put("message", "Your Bank Account has not enough money !!");
                            return "redirect:initError.htm";
                        }
                    } else {
                        mm.put("listMessage", listMessage);
                        return "Frontend/ShopingCart";
                    }
                } else {
                    mm.put("message", "Your Card information is not available !");
                    return "redirect:initCheckOut.htm";
                }
            } else { // Thanh toan truc tiep
                List<String> listMessage = new ArrayList<>();
                String message = "";
                int count = 0; // check xem co false hay ko ma van hien ra thong bao
                for (int i = 0; i < listCart.size(); i++) {
                    Product pro = proModel.getProductByProductId(listCart.get(i).getProduct().getProductId()); // cap nhat lai so luong san pham tren sever
                    listCart.get(i).setProduct(pro);
                    if (listCart.get(i).getQuantity() <= listCart.get(i).getProduct().getQuantity()) {
                        message = "";
                        listMessage.add(message);
                    } else if (listCart.get(i).getProduct().getQuantity() == 0) {
                        message = "The Product has sold out !";
                        listMessage.add(message);
                        count++;
                    } else {
                        message = "The Product has only " + listCart.get(i).getProduct().getQuantity() + " available";
                        listMessage.add(message);
                        count++;
                    }
                }
                mm.put("listMessage", listMessage);
                if (count == 0) {
                    payModel.executeOrder(listCart);
                    boolean check = payModel.createOrder(userName, customerName, address, phone, email, paymentMethod, totalAmount, listCart);
                    if (check) {
                        return "redirect:initSuccess.htm";
                    } else {
                        return "redirect:initError.htm";
                    }
                } else {
                    return "Frontend/ShopingCart";
                }
            }
        } else {
            return "redirect:addCart.htm";
        }
    }

    @RequestMapping(value = "/getOrderHistory")
    public ModelAndView getOrderHistory(HttpSession session) {
        ModelAndView model = new ModelAndView("Frontend/OrderHistory");
        String userName = (String) session.getAttribute("userName");
        List<Orders> listOrders = ordModel.getOrderByUserName(userName);
        model.addObject("listOrders", listOrders);
        return model;
    }

    @RequestMapping(value = "/getOrderDetailByOrderId")
    public ModelAndView getOrderDetailByOrderId(int orderId) {
        ModelAndView model = new ModelAndView("Frontend/OrderDetail");
        List<OrderDetail> listOrderDetail = ordModel.getOrderDetailByOrderId(orderId);
        model.addObject("listOrderDetail", listOrderDetail);
        return model;
    }

    @RequestMapping(value = "/filterPrice")
    public ModelAndView filterPrice(float priceMin, float priceMax) {
        ModelAndView model = new ModelAndView("Frontend/ShopList");
        List<Product> listProduct = proModel.filterPrice(priceMin, priceMax);
        List<Catalog> listCatalog = catModel.getAllCatalog();
        model.addObject("priceMax", priceMax);
        model.addObject("priceMin", priceMin);
        model.addObject("listCatalog", listCatalog);
        model.addObject("listProduct", listProduct);
        return model;
    }
    @RequestMapping(value = "/filterPriceCatalog")
    public ModelAndView filterPriceCatalog(float priceMin, float priceMax,int catalogId) {
        ModelAndView model = new ModelAndView("Frontend/ShopList");
        List<Product> listProduct = proModel.filterPriceCatalog(priceMin, priceMax,catalogId);
        List<Catalog> listCatalog = catModel.getAllCatalog();
        model.addObject("priceMax", priceMax);
        model.addObject("priceMin", priceMin);
        model.addObject("listCatalog", listCatalog);
        model.addObject("listProduct", listProduct);
        model.addObject("catalogId", catalogId);        
        return model;
    }    
}
