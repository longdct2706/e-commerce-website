/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.model;

import com.entities.Catalog;
import com.util.ConnectionDB;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author H
 */
public class CatalogModel {
    public List<Catalog> getAllCatalog(){
        Connection conn=null;
        CallableStatement callSt=null;
        List<Catalog> listCatalog=new ArrayList<>();
        try {
            conn=ConnectionDB.openConnection();
            callSt=conn.prepareCall("{call getAllCatalog()}");
            ResultSet rs=callSt.executeQuery();
            while (rs.next()) {                
                Catalog cat=new Catalog();
                cat.setCatalogId(rs.getInt("CatalogId"));
                cat.setCatalogName(rs.getString("CatalogName"));
                cat.setStatus(rs.getBoolean("Status"));
                listCatalog.add(cat);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally{
            ConnectionDB.closeConnection(conn, callSt);
        }
        return listCatalog;
    }
    public boolean insertCatalog(Catalog cat){
        Connection conn=null;
        CallableStatement callSt=null;
        boolean check;
        try {
            conn=ConnectionDB.openConnection();
            callSt=conn.prepareCall("{call insertCatalog(?,?)}");
            callSt.setString(1, cat.getCatalogName());
            callSt.registerOutParameter(2, Types.BIT);
            callSt.execute();
            check=callSt.getBoolean(2);
        } catch (Exception e) {
            e.printStackTrace();
            check=false;
        } finally{
            ConnectionDB.closeConnection(conn, callSt);
        }
        return check;        
    }
    public boolean updateCatalog(Catalog cat){
        Connection conn=null;
        CallableStatement callSt=null;
        boolean check;
        try {
            conn=ConnectionDB.openConnection();
            callSt=conn.prepareCall("{call updateCatalog(?,?,?,?)}");
            callSt.setInt(1, cat.getCatalogId());
            callSt.setString(2, cat.getCatalogName());
            callSt.setBoolean(3, cat.isStatus());
            callSt.registerOutParameter(4, Types.BIT);
            callSt.execute();
            check=callSt.getBoolean(4);
        } catch (Exception e) {
            e.printStackTrace();
            check=false;
        } finally{
            ConnectionDB.closeConnection(conn, callSt);
        }
        return check;        
    }
    public boolean deleteCatalog(int catalogId){
        Connection conn=null;
        CallableStatement callSt=null;
        boolean check;
        try {
            conn=ConnectionDB.openConnection();
            callSt=conn.prepareCall("{call deleteCatalog(?,?)}");
            callSt.setInt(1, catalogId);
            callSt.registerOutParameter(2, Types.BIT);
            callSt.execute();
            check=callSt.getBoolean(2);
        } catch (Exception e) {
            e.printStackTrace();
            check=false;
        } finally{
            ConnectionDB.closeConnection(conn, callSt);
        }
        return check;        
    } 
    public Catalog getCatalogByCatalogId(int catalogId){
        Connection conn=null;
        CallableStatement callSt=null;
        Catalog cat=new Catalog();
        try {
            conn=ConnectionDB.openConnection();
            callSt=conn.prepareCall("{call getCatalogByCatalogId(?)}");
            callSt.setInt(1, catalogId);
            ResultSet rs=callSt.executeQuery();
            while (rs.next()) {                
                cat.setCatalogId(rs.getInt("CatalogId"));
                cat.setCatalogName(rs.getString("CatalogName"));
                cat.setStatus(rs.getBoolean("Status"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally{
            ConnectionDB.closeConnection(conn, callSt);
        }
        return cat;
    }    
}
