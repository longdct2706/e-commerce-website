/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.model;

import com.entities.OrderDetail;
import com.entities.Orders;
import com.util.ConnectionDB;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author H
 */
public class OrderModel {

    public List<Orders> getAllOrders() {
        SimpleDateFormat fomat = new SimpleDateFormat("dd-MM-yyyy");
        Connection conn = null;
        CallableStatement callSt = null;
        List<Orders> listOrders = new ArrayList<>();
        try {
            conn = ConnectionDB.openConnection();
            callSt = conn.prepareCall("{call getAllOrders}");
            ResultSet rs = callSt.executeQuery();
            while (rs.next()) {
                Orders order = new Orders();
                order.setOrderId(rs.getInt("OrderId"));
                order.setUserName(rs.getString("userName"));
                order.setCustomerName(rs.getString("customerName"));
                order.setTotalAmount(rs.getFloat("totalAmount"));
                order.setAddress(rs.getString("address"));
                order.setPhone(rs.getString("Phone"));
                order.setEmail(rs.getString("email"));
                order.setStatus(rs.getBoolean("Status"));
                if (rs.getDate("CreatedDate") != null) {
                    order.setCreatedDate(fomat.format(rs.getDate("CreatedDate")));
                }
                order.setPaymentMethod(rs.getString("PaymentMethod"));
                listOrders.add(order);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ConnectionDB.closeConnection(conn, callSt);
        }
        return listOrders;
    }

    public List<Orders> getOrderByUserName(String userName) {
        SimpleDateFormat fomat = new SimpleDateFormat("dd-MM-yyyy");
        Connection conn = null;
        CallableStatement callSt = null;
        List<Orders> listOrders = new ArrayList<>();
        try {
            conn = ConnectionDB.openConnection();
            callSt = conn.prepareCall("{call getOrderByUserName(?)}");
            callSt.setString(1, userName);
            ResultSet rs = callSt.executeQuery();
            while (rs.next()) {
                Orders order = new Orders();
                order.setOrderId(rs.getInt("OrderId"));
                order.setUserName(rs.getString("userName"));
                order.setCustomerName(rs.getString("customerName"));
                order.setTotalAmount(rs.getFloat("totalAmount"));
                order.setAddress(rs.getString("address"));
                order.setPhone(rs.getString("Phone"));
                order.setEmail(rs.getString("email"));
                order.setStatus(rs.getBoolean("Status"));
                if (rs.getDate("CreatedDate") != null) {
                    order.setCreatedDate(fomat.format(rs.getDate("CreatedDate")));
                }
                order.setPaymentMethod(rs.getString("PaymentMethod"));
                listOrders.add(order);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ConnectionDB.closeConnection(conn, callSt);
        }
        return listOrders;
    }

    public List<OrderDetail> getOrderDetailByOrderId(int orderId) {
        SimpleDateFormat fomat = new SimpleDateFormat("dd-MM-yyyy");
        Connection conn = null;
        CallableStatement callSt = null;
        List<OrderDetail> listOrderDetail = new ArrayList<>();
        try {
            conn = ConnectionDB.openConnection();
            callSt = conn.prepareCall("{call getOrderDetailByOrderId(?)}");
            callSt.setInt(1, orderId);
            ResultSet rs = callSt.executeQuery();
            while (rs.next()) {
                OrderDetail order = new OrderDetail();
                order.setOrderDetailId(rs.getInt("OrderDetailId"));
                order.setOrderId(rs.getInt("OrderId"));
                order.setPrice(rs.getFloat("Price"));
                order.setProductName(rs.getString("ProductName"));
                order.setQuantity(rs.getInt("Quantity"));
                order.setTotalPrice(rs.getFloat("TotalPrice"));
                if (rs.getDate("CreatedDate") != null) {
                    order.setCreatedDate(fomat.format(rs.getDate("CreatedDate")));
                }
                order.setStatus(rs.getBoolean("Status"));
                listOrderDetail.add(order);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ConnectionDB.closeConnection(conn, callSt);
        }
        return listOrderDetail;
    }

    public List<Orders> getOrderProcess() {
        SimpleDateFormat fomat = new SimpleDateFormat("dd-MM-yyyy");
        Connection conn = null;
        CallableStatement callSt = null;
        List<Orders> listOrder = new ArrayList<>();
        try {
            conn = ConnectionDB.openConnection();
            callSt = conn.prepareCall("{call getOrderProcess}");
            ResultSet rs = callSt.executeQuery();
            while (rs.next()) {
                Orders order = new Orders();
                order.setOrderId(rs.getInt("OrderId"));
                order.setUserName(rs.getString("UserName"));
                order.setCustomerName(rs.getString("CustomerName"));
                order.setEmail(rs.getString("Email"));
                order.setAddress(rs.getString("Address"));
                order.setPhone(rs.getString("Phone"));
                if (rs.getDate("CreatedDate") != null) {
                    order.setCreatedDate(fomat.format(rs.getDate("CreatedDate")));
                }
                order.setPaymentMethod(rs.getString("PaymentMethod"));
                order.setTotalAmount(rs.getFloat("TotalAmount"));
                order.setStatus(rs.getBoolean("Status"));
                listOrder.add(order);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ConnectionDB.closeConnection(conn, callSt);
        }
        return listOrder;
    }

    public List<Orders> getAllOrder() {
        SimpleDateFormat fomat = new SimpleDateFormat("dd-MM-yyyy");
        Connection conn = null;
        CallableStatement callSt = null;
        List<Orders> listOrder = new ArrayList<>();
        try {
            conn = ConnectionDB.openConnection();
            callSt = conn.prepareCall("{call getAllOrder}");
            ResultSet rs = callSt.executeQuery();
            while (rs.next()) {
                Orders order = new Orders();
                order.setOrderId(rs.getInt("OrderId"));
                order.setUserName(rs.getString("UserName"));
                order.setCustomerName(rs.getString("CustomerName"));
                order.setEmail(rs.getString("Email"));
                order.setAddress(rs.getString("Address"));
                order.setPhone(rs.getString("Phone"));
                if (rs.getDate("CreatedDate") != null) {
                    order.setCreatedDate(fomat.format(rs.getDate("CreatedDate")));
                }
                order.setPaymentMethod(rs.getString("PaymentMethod"));
                order.setTotalAmount(rs.getFloat("TotalAmount"));
                order.setStatus(rs.getBoolean("Status"));
                listOrder.add(order);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ConnectionDB.closeConnection(conn, callSt);
        }
        return listOrder;
    }

    public boolean deleteOrder(int orderId) {
        Connection conn = null;
        CallableStatement callSt = null;
        boolean check;
        try {
            conn = ConnectionDB.openConnection();
            callSt = conn.prepareCall("{call deleteOrder(?,?)}");
            callSt.setInt(1, orderId);
            callSt.registerOutParameter(2, Types.BIT);
            callSt.execute();
            check = callSt.getBoolean(2);
        } catch (Exception e) {
            e.printStackTrace();
            check = false;
        } finally {
            ConnectionDB.closeConnection(conn, callSt);
        }
        return check;
    }

    public boolean recoverQuantity(List<OrderDetail> listOrderDetail) {
        Connection conn = null;
        CallableStatement callSt = null;
        boolean check = false;
        try {
            conn = ConnectionDB.openConnection();
            for (OrderDetail order : listOrderDetail) {
                callSt = conn.prepareCall("{call recoverQuantity(?,?,?)}");
                callSt.setString(1, order.getProductName());
                callSt.setInt(2, order.getQuantity());
                callSt.registerOutParameter(3, Types.BIT);
                callSt.execute();
                check = callSt.getBoolean(3);
                if (check == false) {
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            check = false;
        } finally {
            ConnectionDB.closeConnection(conn, callSt);
        }
        return check;
    }

    public boolean updateOrder(Orders order) {
        Connection conn = null;
        CallableStatement callSt = null;
        boolean check;
        try {
            conn = ConnectionDB.openConnection();
            callSt = conn.prepareCall("{call updateOrder(?,?,?,?,?,?,?,?,?)}");
            callSt.setInt(1, order.getOrderId());
            callSt.setString(2, order.getCustomerName());
            callSt.setFloat(3, order.getTotalAmount());
            callSt.setString(4, order.getAddress());
            callSt.setString(5, order.getPhone());
            callSt.setString(6, order.getEmail());
            callSt.setString(7, order.getPaymentMethod());
            callSt.setBoolean(8, order.isStatus());
            callSt.registerOutParameter(9, Types.BIT);
            callSt.execute();
            check = callSt.getBoolean(9);
        } catch (Exception e) {
            e.printStackTrace();
            check = false;
        } finally {
            ConnectionDB.closeConnection(conn, callSt);
        }
        return check;
    }

    public Orders getOrderByOrderId(int orderId) {
        SimpleDateFormat fomat = new SimpleDateFormat("dd-MM-yyyy");
        Connection conn = null;
        CallableStatement callSt = null;
        Orders order = new Orders();
        try {
            conn = ConnectionDB.openConnection();
            callSt = conn.prepareCall("{call getOrderByOrderId(?)}");
            callSt.setInt(1, orderId);
            ResultSet rs = callSt.executeQuery();
            while (rs.next()) {
                order.setOrderId(rs.getInt("OrderId"));
                order.setUserName(rs.getString("UserName"));
                order.setCustomerName(rs.getString("CustomerName"));
                order.setEmail(rs.getString("Email"));
                order.setAddress(rs.getString("Address"));
                order.setPhone(rs.getString("Phone"));
                if (rs.getDate("CreatedDate") != null) {
                    order.setCreatedDate(fomat.format(rs.getDate("CreatedDate")));
                }
                order.setPaymentMethod(rs.getString("PaymentMethod"));
                order.setTotalAmount(rs.getFloat("TotalAmount"));
                order.setStatus(rs.getBoolean("Status"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ConnectionDB.closeConnection(conn, callSt);
        }
        return order;
    }

    public List<OrderDetail> getListIncome(int month){
        SimpleDateFormat fomat = new SimpleDateFormat("dd-MM-yyyy");
        Connection conn = null;
        CallableStatement callSt = null;
        List<OrderDetail> listOrderDetail = new ArrayList<>();
        try {
            conn = ConnectionDB.openConnection();
            callSt = conn.prepareCall("{call getListIncome(?)}");
            callSt.setInt(1, month);
            ResultSet rs = callSt.executeQuery();
            while (rs.next()) {
                OrderDetail order = new OrderDetail();
                order.setProductName(rs.getString(1));
                order.setPrice(rs.getFloat(2));                
                order.setQuantity(rs.getInt(3));
                order.setTotalPrice(rs.getFloat(4));
                listOrderDetail.add(order);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ConnectionDB.closeConnection(conn, callSt);
        }
        return listOrderDetail;        
    }
}
