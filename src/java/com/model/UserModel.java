/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.model;

import com.entities.Users;
import com.util.ConnectionDB;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author H
 */
public class UserModel {

    public List<Users> getAllUsers() {
        SimpleDateFormat fomat = new SimpleDateFormat("dd-MM-yyyy");
        Connection conn = null;
        CallableStatement callSt = null;
        List<Users> listUsers = new ArrayList<>();
        try {
            conn = ConnectionDB.openConnection();
            callSt = conn.prepareCall("{call getAllUsers}");
            ResultSet rs = callSt.executeQuery();
            while (rs.next()) {
                Users user = new Users();
                user.setUserId(rs.getInt("UserId"));
                user.setFullName(rs.getString("FullName"));
                user.setUserName(rs.getString("UserName"));
                user.setPass(rs.getString("Pass"));
                user.setEmail(rs.getString("Email"));
                user.setAddress(rs.getString("Address"));
                user.setPhone(rs.getString("Phone"));
                if (rs.getDate("Created") != null) {
                    user.setCreated(fomat.format(rs.getDate("Created")));
                }
                user.setIsAdmin(rs.getBoolean("IsAdmin"));
                user.setStatus(rs.getBoolean("Status"));
                listUsers.add(user);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ConnectionDB.closeConnection(conn, callSt);
        }
        return listUsers;
    }

    public Users getUserByUserId(int userId) {
        SimpleDateFormat fomat = new SimpleDateFormat("dd-MM-yyyy");
        Connection conn = null;
        CallableStatement callSt = null;
        Users user = new Users();
        try {
            conn = ConnectionDB.openConnection();
            callSt = conn.prepareCall("{call getUserByUserId(?)}");
            callSt.setInt(1, userId);
            ResultSet rs = callSt.executeQuery();
            while (rs.next()) {
                user.setUserId(rs.getInt("UserId"));
                user.setFullName(rs.getString("FullName"));
                user.setUserName(rs.getString("UserName"));
                user.setPass(rs.getString("Pass"));
                user.setEmail(rs.getString("Email"));
                user.setAddress(rs.getString("Address"));
                user.setPhone(rs.getString("Phone"));
                if (rs.getDate("Created") != null) {
                    user.setCreated(fomat.format(rs.getDate("Created")));
                }
                user.setIsAdmin(rs.getBoolean("IsAdmin"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ConnectionDB.closeConnection(conn, callSt);
        }
        return user;
    }

    public Users getUserByUserName(String userName) {
        SimpleDateFormat fomat = new SimpleDateFormat("dd-MM-yyyy");
        Connection conn = null;
        CallableStatement callSt = null;
        Users user = new Users();
        try {
            conn = ConnectionDB.openConnection();
            callSt = conn.prepareCall("{call checkUsername(?)}");
            callSt.setString(1, userName);
            ResultSet rs = callSt.executeQuery();
            while (rs.next()) {
                user.setUserId(rs.getInt("UserId"));
                user.setFullName(rs.getString("FullName"));
                user.setUserName(rs.getString("UserName"));
                user.setPass(rs.getString("Pass"));
                user.setEmail(rs.getString("Email"));
                user.setAddress(rs.getString("Address"));
                user.setPhone(rs.getString("Phone"));
                if (rs.getDate("Created") != null) {
                    user.setCreated(fomat.format(rs.getDate("Created")));
                }
                user.setIsAdmin(rs.getBoolean("IsAdmin"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ConnectionDB.closeConnection(conn, callSt);
        }
        return user;
    }

    public boolean insertUsers(Users user) {
        Connection conn = null;
        CallableStatement callSt = null;
        boolean check;
        try {
            conn = ConnectionDB.openConnection();
            callSt = conn.prepareCall("{call insertUsers(?,?,?,?,?,?,?,?)}");
            callSt.setString(1, user.getFullName());
            callSt.setString(2, user.getUserName());
            callSt.setString(3, user.getPass());
            callSt.setString(4, user.getEmail());
            callSt.setString(5, user.getAddress());
            callSt.setString(6, user.getPhone());
            callSt.setBoolean(7, user.isIsAdmin());
            callSt.registerOutParameter(8, Types.BIT);
            callSt.execute();
            check = callSt.getBoolean(8);
        } catch (Exception e) {
            e.printStackTrace();
            check = false;
        } finally {
            ConnectionDB.closeConnection(conn, callSt);
        }
        return check;
    }

    public boolean updateUsers(Users user) {
        Connection conn = null;
        CallableStatement callSt = null;
        boolean check;
        try {
            conn = ConnectionDB.openConnection();
            callSt = conn.prepareCall("{call updateUsers(?,?,?,?,?,?,?,?,?,?)}");
            callSt.setInt(1, user.getUserId());
            callSt.setString(2, user.getFullName());
            callSt.setString(3, user.getUserName());
            callSt.setString(4, user.getPass());
            callSt.setString(5, user.getEmail());
            callSt.setString(6, user.getAddress());
            callSt.setString(7, user.getPhone());
            callSt.setBoolean(8, user.isIsAdmin());
            callSt.setBoolean(9, user.isStatus());
            callSt.registerOutParameter(10, Types.BIT);
            callSt.execute();
            check = callSt.getBoolean(10);
        } catch (Exception e) {
            e.printStackTrace();
            check = false;
        } finally {
            ConnectionDB.closeConnection(conn, callSt);
        }
        return check;
    }

    public boolean deleteUsers(int userId) {
        Connection conn = null;
        CallableStatement callSt = null;
        boolean check;
        try {
            conn = ConnectionDB.openConnection();
            callSt = conn.prepareCall("{call deleteUsers(?,?)}");
            callSt.setInt(1, userId);
            callSt.registerOutParameter(2, Types.BIT);
            callSt.execute();
            check = callSt.getBoolean(2);
        } catch (Exception e) {
            e.printStackTrace();
            check = false;
        } finally {
            ConnectionDB.closeConnection(conn, callSt);
        }
        return check;
    }

    public boolean checkLogin(String userName, String pass) {
        Connection conn = null;
        CallableStatement callSt = null;
        boolean check = false;
        try {
            conn = ConnectionDB.openConnection();
            callSt = conn.prepareCall("{call checkLogin(?,?)}");
            callSt.setString(1, userName);
            callSt.setString(2, pass);
            ResultSet rs = callSt.executeQuery();
            while (rs.next()) {
                check = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ConnectionDB.closeConnection(conn, callSt);
        }
        return check;
    }

    public boolean checkUsername(String userName) {
        Connection conn = null;
        CallableStatement callSt = null;
        boolean check = false;
        try {
            conn = ConnectionDB.openConnection();
            callSt = conn.prepareCall("{call checkUsername(?)}");
            callSt.setString(1, userName);
            ResultSet rs = callSt.executeQuery();
            while (rs.next()) {
                check = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ConnectionDB.closeConnection(conn, callSt);
        }
        return check;
    }

    public boolean checkEmail(String email) {
        Connection conn = null;
        CallableStatement callSt = null;
        boolean check = false;
        try {
            conn = ConnectionDB.openConnection();
            callSt = conn.prepareCall("{call checkEmail(?)}");
            callSt.setString(1, email);
            ResultSet rs = callSt.executeQuery();
            while (rs.next()) {
                check = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ConnectionDB.closeConnection(conn, callSt);
        }
        return check;
    }

    public boolean registerUser(Users user) {
        Connection conn = null;
        CallableStatement callSt = null;
        boolean check;
        try {
            conn = ConnectionDB.openConnection();
            callSt = conn.prepareCall("{call registerUser(?,?,?,?,?,?,?)}");
            callSt.setString(1, user.getFullName());
            callSt.setString(2, user.getUserName());
            callSt.setString(3, user.getPass());
            callSt.setString(4, user.getEmail());
            callSt.setString(5, user.getAddress());
            callSt.setString(6, user.getPhone());
            callSt.registerOutParameter(7, Types.BIT);
            callSt.execute();
            check = callSt.getBoolean(7);
        } catch (Exception e) {
            e.printStackTrace();
            check = false;
        } finally {
            ConnectionDB.closeConnection(conn, callSt);
        }
        return check;
    }

    public List<Users> getListCustomer() {
        SimpleDateFormat fomat = new SimpleDateFormat("dd-MM-yyyy");
        Connection conn = null;
        CallableStatement callSt = null;
        List<Users> listUsers = new ArrayList<>();
        try {
            conn = ConnectionDB.openConnection();
            callSt = conn.prepareCall("{call getListCustomer}");
            ResultSet rs = callSt.executeQuery();
            while (rs.next()) {
                Users user = new Users();
                user.setUserId(rs.getInt("UserId"));
                user.setFullName(rs.getString("FullName"));
                user.setUserName(rs.getString("UserName"));
                user.setPass(rs.getString("Pass"));
                user.setEmail(rs.getString("Email"));
                user.setAddress(rs.getString("Address"));
                user.setPhone(rs.getString("Phone"));
                if (rs.getDate("Created") != null) {
                    user.setCreated(fomat.format(rs.getDate("Created")));
                }
                user.setIsAdmin(rs.getBoolean("IsAdmin"));
                user.setStatus(rs.getBoolean("Status"));
                listUsers.add(user);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ConnectionDB.closeConnection(conn, callSt);
        }
        return listUsers;
    }

    public boolean checkLoginAdmin(String userName, String pass) {
        Connection conn = null;
        CallableStatement callSt = null;
        boolean check = false;
        try {
            conn = ConnectionDB.openConnection();
            callSt = conn.prepareCall("{call checkLoginAdmin(?,?)}");
            callSt.setString(1, userName);
            callSt.setString(2, pass);
            ResultSet rs = callSt.executeQuery();
            while (rs.next()) {
                check = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ConnectionDB.closeConnection(conn, callSt);
        }
        return check;
    }
}
