/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.model;

import com.entities.Product;
import com.util.ConnectionDB;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author H
 */
public class ProductModel {

    public List<Product> getAllProduct() {
        SimpleDateFormat fomat = new SimpleDateFormat("dd-MM-yyyy");
        Connection conn = null;
        CallableStatement callSt = null;
        List<Product> listProduct = new ArrayList<>();
        try {
            conn = ConnectionDB.openConnection();
            callSt = conn.prepareCall("{call getAllProduct()}");
            ResultSet rs = callSt.executeQuery();
            while (rs.next()) {
                Product pro = new Product();
                pro.setProductId(rs.getInt("ProductId"));
                pro.setProductName(rs.getString("ProductName"));
                pro.setProductContent(rs.getString("ProductContent"));
                pro.setPrice(rs.getFloat("Price"));
                pro.setQuantity(rs.getInt("Quantity"));
                pro.setImages(rs.getString("Images")); // 
                pro.setCatalogId(rs.getInt("CatalogId"));
                if (rs.getDate("Created") != null) {
                    pro.setCreated(fomat.format(rs.getDate("Created")));
                }
                pro.setImageHover(rs.getString("ImageHover"));
                pro.setStatus(rs.getBoolean("Status"));
                pro.setDiscount(rs.getInt("Discount"));
                listProduct.add(pro);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ConnectionDB.closeConnection(conn, callSt);
        }
        return listProduct;
    }

    public boolean insertProduct(Product pro) {
        Connection conn = null;
        CallableStatement callSt = null;
        boolean check;
        try {
            conn = ConnectionDB.openConnection();
            callSt = conn.prepareCall("{call insertProduct(?,?,?,?,?,?,?,?,?)}");
            callSt.setString(1, pro.getProductName());
            callSt.setString(2, pro.getProductContent());
            callSt.setString(3, pro.getImages());
            callSt.setFloat(4, pro.getPrice());
            callSt.setInt(5, pro.getQuantity());
            callSt.setInt(6, pro.getCatalogId());
            callSt.setString(7, pro.getImageHover());
            callSt.setInt(8, pro.getDiscount());
            callSt.registerOutParameter(9, Types.BIT);
            callSt.execute();
            check = callSt.getBoolean(9);
        } catch (Exception e) {
            e.printStackTrace();
            check = false;
        } finally {
            ConnectionDB.closeConnection(conn, callSt);
        }
        return check;
    }

    public Product getProductByProductId(int productId) {
        SimpleDateFormat fomat = new SimpleDateFormat("dd-MM-yyyy");
        Connection conn = null;
        CallableStatement callSt = null;
        Product pro = new Product();
        try {
            conn = ConnectionDB.openConnection();
            callSt = conn.prepareCall("{call getProductByProductId(?)}");
            callSt.setInt(1, productId);
            ResultSet rs = callSt.executeQuery();
            while (rs.next()) {
                pro.setProductId(rs.getInt("ProductId"));
                pro.setProductName(rs.getString("ProductName"));
                pro.setProductContent(rs.getString("ProductContent"));
                pro.setPrice(rs.getFloat("Price"));
                pro.setQuantity(rs.getInt("Quantity"));
                pro.setImages(rs.getString("Images")); // 
                pro.setCatalogId(rs.getInt("CatalogId"));
                if (rs.getDate("Created") != null) {
                    pro.setCreated(fomat.format(rs.getDate("Created")));
                }
                pro.setImageHover(rs.getString("ImageHover"));
                pro.setStatus(rs.getBoolean("Status"));
                pro.setDiscount(rs.getInt("Discount"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ConnectionDB.closeConnection(conn, callSt);
        }
        return pro;
    }

    public boolean updateProduct(Product pro) {
        Connection conn = null;
        CallableStatement callSt = null;
        boolean check;
        try {
            conn = ConnectionDB.openConnection();
            callSt = conn.prepareCall("{call updateProduct(?,?,?,?,?,?,?,?,?,?,?)}");
            callSt.setInt(1, pro.getProductId());
            callSt.setString(2, pro.getProductName());
            callSt.setString(3, pro.getProductContent());
            callSt.setString(4, pro.getImages());
            callSt.setFloat(5, pro.getPrice());
            callSt.setInt(6, pro.getQuantity());
            callSt.setInt(7, pro.getCatalogId());
            callSt.setBoolean(8, pro.isStatus());
            callSt.setString(9, pro.getImageHover());
            callSt.setInt(10, pro.getDiscount());
            callSt.registerOutParameter(11, Types.BIT);
            callSt.execute();
            check = callSt.getBoolean(11);
        } catch (Exception e) {
            e.printStackTrace();
            check = false;
        } finally {
            ConnectionDB.closeConnection(conn, callSt);
        }
        return check;
    }

    public boolean deleteProduct(int productId) {
        Connection conn = null;
        CallableStatement callSt = null;
        boolean check;
        try {
            conn = ConnectionDB.openConnection();
            callSt = conn.prepareCall("{call deleteProduct(?,?)}");
            callSt.setInt(1, productId);
            callSt.registerOutParameter(2, Types.BIT);
            callSt.execute();
            check = callSt.getBoolean(2);
        } catch (Exception e) {
            e.printStackTrace();
            check = false;
        } finally {
            ConnectionDB.closeConnection(conn, callSt);
        }
        return check;
    }

    public List<Product> getProductByCatalogId(int catalogId) {
        SimpleDateFormat fomat = new SimpleDateFormat("dd-MM-yyyy");
        Connection conn = null;
        CallableStatement callSt = null;
        List<Product> listProduct = new ArrayList<>();
        try {
            conn = ConnectionDB.openConnection();
            callSt = conn.prepareCall("{call getProductByCatalogId(?)}");
            callSt.setInt(1, catalogId);
            ResultSet rs = callSt.executeQuery();
            while (rs.next()) {
                Product pro = new Product();
                pro.setProductId(rs.getInt("ProductId"));
                pro.setProductName(rs.getString("ProductName"));
                pro.setProductContent(rs.getString("ProductContent"));
                pro.setPrice(rs.getFloat("Price"));
                pro.setQuantity(rs.getInt("Quantity"));
                pro.setImages(rs.getString("Images")); // 
                pro.setCatalogId(rs.getInt("CatalogId"));
                if (rs.getDate("Created") != null) {
                    pro.setCreated(fomat.format(rs.getDate("Created")));
                }
                pro.setImageHover(rs.getString("ImageHover"));
                pro.setStatus(rs.getBoolean("Status"));
                pro.setDiscount(rs.getInt("Discount"));
                listProduct.add(pro);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ConnectionDB.closeConnection(conn, callSt);
        }
        return listProduct;
    }
    
    public List<Product> getBestProduct(){
        SimpleDateFormat fomat=new SimpleDateFormat("dd-MM-yyyy");
        Connection conn=null;
        CallableStatement callSt=null;
        List<Product> listProduct=new ArrayList<>();
        try {
            conn=ConnectionDB.openConnection();
            callSt=conn.prepareCall("{call getBestProduct}");
            ResultSet rs=callSt.executeQuery();
            while(rs.next()){
                Product pro=new Product();
                pro.setProductId(rs.getInt("ProductId"));
                pro.setProductName(rs.getString("ProductName"));
                pro.setPrice(rs.getFloat("Price"));
                pro.setProductContent(rs.getString("ProductContent"));
                pro.setImages(rs.getString("Images"));
                pro.setImageHover(rs.getString("ImageHover"));
                pro.setCatalogId(rs.getInt("CatalogId"));
                pro.setQuantity(rs.getInt("Quantity"));
                if(rs.getDate("Created")!=null){
                    pro.setCreated(fomat.format(rs.getDate("Created")));
                }
                pro.setStatus(rs.getBoolean("Status"));
                pro.setDiscount(rs.getInt("Discount"));
                listProduct.add(pro);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally{
            ConnectionDB.closeConnection(conn, callSt);
        }
        return listProduct;
    }
    public List<Product> filterPrice(float priceMin,float priceMax){
        SimpleDateFormat fomat=new SimpleDateFormat("dd-MM-yyyy");
        Connection conn=null;
        CallableStatement callSt=null;
        List<Product> listProduct=new ArrayList<>();
        try {
            conn=ConnectionDB.openConnection();
            callSt=conn.prepareCall("{call filterPrice(?,?)}");
            callSt.setFloat(1, priceMin);
            callSt.setFloat(2, priceMax);
            ResultSet rs=callSt.executeQuery();
            while(rs.next()){
                Product pro=new Product();
                pro.setProductId(rs.getInt("ProductId"));
                pro.setProductName(rs.getString("ProductName"));
                pro.setPrice(rs.getFloat("Price"));
                pro.setProductContent(rs.getString("ProductContent"));
                pro.setImages(rs.getString("Images"));
                pro.setImageHover(rs.getString("ImageHover"));
                pro.setCatalogId(rs.getInt("CatalogId"));
                pro.setQuantity(rs.getInt("Quantity"));
                if(rs.getDate("Created")!=null){
                    pro.setCreated(fomat.format(rs.getDate("Created")));
                }
                pro.setStatus(rs.getBoolean("Status"));
                pro.setDiscount(rs.getInt("Discount"));
                listProduct.add(pro);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally{
            ConnectionDB.closeConnection(conn, callSt);
        }
        return listProduct;
    }
    public List<Product> filterPriceCatalog(float priceMin,float priceMax,int catalogId){
        SimpleDateFormat fomat=new SimpleDateFormat("dd-MM-yyyy");
        Connection conn=null;
        CallableStatement callSt=null;
        List<Product> listProduct=new ArrayList<>();
        try {
            conn=ConnectionDB.openConnection();
            callSt=conn.prepareCall("{call filterPriceCatalog(?,?,?)}");
            callSt.setFloat(1, priceMin);
            callSt.setFloat(2, priceMax);
            callSt.setInt(3, catalogId);
            ResultSet rs=callSt.executeQuery();
            while(rs.next()){
                Product pro=new Product();
                pro.setProductId(rs.getInt("ProductId"));
                pro.setProductName(rs.getString("ProductName"));
                pro.setPrice(rs.getFloat("Price"));
                pro.setProductContent(rs.getString("ProductContent"));
                pro.setImages(rs.getString("Images"));
                pro.setImageHover(rs.getString("ImageHover"));
                pro.setCatalogId(rs.getInt("CatalogId"));
                pro.setQuantity(rs.getInt("Quantity"));
                if(rs.getDate("Created")!=null){
                    pro.setCreated(fomat.format(rs.getDate("Created")));
                }
                pro.setStatus(rs.getBoolean("Status"));
                pro.setDiscount(rs.getInt("Discount"));
                listProduct.add(pro);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally{
            ConnectionDB.closeConnection(conn, callSt);
        }
        return listProduct;
    } 
    public List<Product> getProductRunningOut() {
        SimpleDateFormat fomat = new SimpleDateFormat("dd-MM-yyyy");
        Connection conn = null;
        CallableStatement callSt = null;
        List<Product> listProduct = new ArrayList<>();
        try {
            conn = ConnectionDB.openConnection();
            callSt = conn.prepareCall("{call getProductRunningOut()}");
            ResultSet rs = callSt.executeQuery();
            while (rs.next()) {
                Product pro = new Product();
                pro.setProductId(rs.getInt("ProductId"));
                pro.setProductName(rs.getString("ProductName"));
                pro.setProductContent(rs.getString("ProductContent"));
                pro.setPrice(rs.getFloat("Price"));
                pro.setQuantity(rs.getInt("Quantity"));
                pro.setImages(rs.getString("Images")); // 
                pro.setCatalogId(rs.getInt("CatalogId"));
                if (rs.getDate("Created") != null) {
                    pro.setCreated(fomat.format(rs.getDate("Created")));
                }
                pro.setImageHover(rs.getString("ImageHover"));
                pro.setStatus(rs.getBoolean("Status"));
                pro.setDiscount(rs.getInt("Discount"));
                listProduct.add(pro);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ConnectionDB.closeConnection(conn, callSt);
        }
        return listProduct;
    }    
}
