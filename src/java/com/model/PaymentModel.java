/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.model;

import com.entities.Cart;
import com.util.ConnectionDB;
import com.util.PaymentDB;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.List;

/**
 *
 * @author H
 */
public class PaymentModel {

    public boolean checkPayment(String accountName, String cardNumber) {
        Connection conn = null;
        CallableStatement callSt = null;
        boolean check = false;
        try {
            conn = PaymentDB.openConnection();
            callSt = conn.prepareCall("{call checkPayment(?,?)}");
            callSt.setString(1, accountName);
            callSt.setString(2, cardNumber);
            ResultSet rs = callSt.executeQuery();
            while (rs.next()) {
                check = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            PaymentDB.closeConnection(conn, callSt);
        }
        return check;
    }

    public int getAccountIdByCardNumber(String cardNumber) {
        Connection conn = null;
        CallableStatement callSt = null;
        int accountId = 0;
        try {
            conn = PaymentDB.openConnection();
            callSt = conn.prepareCall("{call getAccountIdByCardNumber(?)}");
            callSt.setString(1, cardNumber);
            ResultSet rs = callSt.executeQuery();
            while (rs.next()) {
                accountId = rs.getInt("AccountId");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            PaymentDB.closeConnection(conn, callSt);
        }
        return accountId;
    }

    // Chuyen Khoan

    public boolean executePayment(int accountId, float totalAmount) {
        Connection conn = null;
        CallableStatement callSt = null;
        boolean check = false;
        try {
            conn = PaymentDB.openConnection();
            callSt = conn.prepareCall("{call executePayment(?,?,?)}");
            callSt.setInt(1, accountId);
            callSt.setFloat(2, totalAmount);
            callSt.registerOutParameter(3, Types.BIT);
            callSt.execute();
            check = callSt.getBoolean(3);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            PaymentDB.closeConnection(conn, callSt);
        }
        return check;
    }

    public boolean executeOrder(List<Cart> listCart) {
        Connection conn = null;
        CallableStatement callSt = null;
        boolean check = false;
        try {
            conn = ConnectionDB.openConnection();
            for (Cart cart : listCart) {
                callSt = conn.prepareCall("{call executeOrder(?,?,?)}");
                callSt.setInt(1, cart.getProduct().getProductId());
                callSt.setInt(2, cart.getQuantity());
                callSt.registerOutParameter(3, Types.BIT);
                callSt.execute();
                check = callSt.getBoolean(3);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            PaymentDB.closeConnection(conn, callSt);
        }
        return check;
    }

    public boolean recoverOrder(List<Cart> listCart) {
        Connection conn = null;
        CallableStatement callSt = null;
        boolean check = false;
        try {
            conn = ConnectionDB.openConnection();
            for (Cart cart : listCart) {
                callSt = conn.prepareCall("{call recoverOrder(?,?,?)}");
                callSt.setInt(1, cart.getProduct().getProductId());
                callSt.setInt(2, cart.getQuantity());
                callSt.registerOutParameter(3, Types.BIT);
                callSt.execute();
                check = callSt.getBoolean(3);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            PaymentDB.closeConnection(conn, callSt);
        }
        return check;
    }

    public boolean createOrder(String userName, String customerName, String address, String phone, String email, String paymentMethod, float totalAmount, List<Cart> listCart) {
        Connection conn = null;
        CallableStatement callSt = null;
        boolean check;
        int orderId = 0;
        try {
            conn = ConnectionDB.openConnection();
            callSt = conn.prepareCall("{call createOrder(?,?,?,?,?,?,?,?)}");
            callSt.setString(1, userName);
            callSt.setString(2, customerName);
            callSt.setFloat(3, totalAmount);
            callSt.setString(4, address);
            callSt.setString(5, phone);
            callSt.setString(6, email);
            callSt.setString(7, paymentMethod);
            callSt.registerOutParameter(8, Types.BIT);
            callSt.execute();
            check = callSt.getBoolean(8);
            if (check == false) {
                return check;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            ConnectionDB.closeConnection(conn, callSt);
        }
        try {
            conn = ConnectionDB.openConnection();
            callSt = conn.prepareCall("{call getNewestOrder}");
            ResultSet rs = callSt.executeQuery();
            while (rs.next()) {
                orderId = rs.getInt("OrderId");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            ConnectionDB.closeConnection(conn, callSt);
        }
        try {
            conn = ConnectionDB.openConnection();
            for (Cart cart : listCart) {
                callSt = conn.prepareCall("{call createOrderDetail(?,?,?,?,?,?)}");
                callSt.setString(1, cart.getProduct().getProductName());
                callSt.setInt(2, orderId);
                callSt.setFloat(3, cart.getProduct().getPrice() * (100 - cart.getProduct().getDiscount()) / 100);
                callSt.setInt(4, cart.getQuantity());
                callSt.setFloat(5, cart.getProduct().getPrice() * (100 - cart.getProduct().getDiscount()) * cart.getQuantity() / 100);
                callSt.registerOutParameter(6, Types.BIT);
                callSt.execute();
                check = callSt.getBoolean(6);
                if (check == false) {
                    return check;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            ConnectionDB.closeConnection(conn, callSt);
        }
        return check;
    }
}
