/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.entities;

import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author H
 */
public class ProductUpdate {
    private int productId;
    private String productName;
    private String productContent;
    private String images;
    private MultipartFile imagesUpdate;
    private float price;
    private int quantity;
    private int catalogId;
    private String created;
    private String imageHover;
    private MultipartFile imageHoverUpdate;
    private int discount;
    private boolean status; 

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductContent() {
        return productContent;
    }

    public void setProductContent(String productContent) {
        this.productContent = productContent;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public MultipartFile getImagesUpdate() {
        return imagesUpdate;
    }

    public void setImagesUpdate(MultipartFile imagesUpdate) {
        this.imagesUpdate = imagesUpdate;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getCatalogId() {
        return catalogId;
    }

    public void setCatalogId(int catalogId) {
        this.catalogId = catalogId;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getImageHover() {
        return imageHover;
    }

    public void setImageHover(String imageHover) {
        this.imageHover = imageHover;
    }

    public MultipartFile getImageHoverUpdate() {
        return imageHoverUpdate;
    }

    public void setImageHoverUpdate(MultipartFile imageHoverUpdate) {
        this.imageHoverUpdate = imageHoverUpdate;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

}
